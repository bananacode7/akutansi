/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100137
Source Host           : 127.0.0.1:3306
Source Database       : manujeku_keuangan

Target Server Type    : MYSQL
Target Server Version : 100137
File Encoding         : 65001

Date: 2021-02-04 18:51:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for keuangan_contact
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_contact`;
CREATE TABLE `keuangan_contact` (
  `contact_id` int(2) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(100) NOT NULL,
  `contact_address` text NOT NULL,
  `contact_phone` varchar(15) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `contact_web` varchar(50) DEFAULT NULL,
  `contact_update` datetime NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_contact
-- ----------------------------
INSERT INTO `keuangan_contact` VALUES ('1', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Jl. Simpang Tujuh No 1 - Kudus', '0291 - 444164', 'bkpp@kuduskab.go.id', 'http://bkpp.kuduskab.go.id', '2020-04-08 09:12:56');

-- ----------------------------
-- Table structure for keuangan_log
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_log`;
CREATE TABLE `keuangan_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `log_user` varchar(30) NOT NULL,
  `log_date` datetime NOT NULL,
  `log_ip` varchar(30) NOT NULL,
  `log_agent` varchar(100) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_log
-- ----------------------------
INSERT INTO `keuangan_log` VALUES ('190', 'admin', '2021-02-03 20:36:21', '::1', 'Chrome 88.0.4324.104');
INSERT INTO `keuangan_log` VALUES ('191', 'admin', '2021-02-03 20:39:05', '::1', 'Chrome 88.0.4324.104');
INSERT INTO `keuangan_log` VALUES ('192', 'admin', '2021-02-04 17:58:30', '::1', 'Chrome 88.0.4324.104');

-- ----------------------------
-- Table structure for keuangan_master_reg
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_master_reg`;
CREATE TABLE `keuangan_master_reg` (
  `masreg_id` int(11) NOT NULL AUTO_INCREMENT,
  `masreg_kode` varchar(100) DEFAULT NULL,
  `masreg_nama` varchar(255) DEFAULT NULL,
  `masreg_index` varchar(100) DEFAULT NULL,
  `masreg_sub` varchar(100) DEFAULT NULL,
  `masreg_level` varchar(100) DEFAULT NULL,
  `masreg_update` timestamp NULL DEFAULT NULL,
  `masreg_status` enum('aktif','tidak') NOT NULL,
  PRIMARY KEY (`masreg_id`),
  UNIQUE KEY `keuangan_master_reg_masreg_id_IDX` (`masreg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_master_reg
-- ----------------------------
INSERT INTO `keuangan_master_reg` VALUES ('2', '1', 'Pendapatan', 'head', '', '0', '2021-01-26 23:57:40', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('9', '1.01', 'Dana Hibah', '', '1', '1', '2021-01-28 04:00:38', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('10', '1.02', 'Dana Yayasan', '', '1', '1', '2021-01-28 04:00:57', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('13', '1.03', 'sumbangan lain lain', '', '1', '1', '2021-01-28 04:20:46', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('15', '1.04', 'sumbangan orang tua', '', '1', '1', '2021-01-28 04:21:24', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('16', '1.02.01', 'dana dana', '', '1.02', '2', '2021-01-31 21:55:16', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('17', '1.02.02', 'dana lanjutan', '', '1.02', '2', '2021-01-31 21:55:40', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('18', '1.02.03', 'dana kelanjutan', '', '1.02', '2', '2021-01-31 21:55:55', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('19', '1.04.01', 'dana wali murid', '', '1.04', '2', '2021-01-31 21:56:24', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('21', '2', 'Belanja', 'head', '', '0', '2021-01-31 21:57:14', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('23', '2.01', 'Belanja Atk', '', '2', '1', '2021-01-31 21:58:09', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('24', '2.02', 'Belanja OSIS', '', '2', '1', '2021-01-31 21:58:23', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('25', '2.01.01', 'Kelengkapan Administrasi', '', '2.01', '2', '2021-01-31 21:58:54', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('27', '3', 'Hutang', 'head', '', '0', '2021-02-01 15:27:53', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('28', '3.01', 'Hutang yayasan', '', '3', '1', '2021-02-01 15:28:30', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('29', '3.02', 'Hutang bank', '', '3', '1', '2021-02-01 15:29:24', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('30', '3.01.01', 'Hutang yayasan 2020', '', '3.01', '2', '2021-02-01 15:29:53', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('31', '3.01.02', 'Hutang yayasan 2021', '', '3.01', '2', '2021-02-01 15:30:14', 'aktif');
INSERT INTO `keuangan_master_reg` VALUES ('32', '3.01.03', 'Hutang yayasan 2022', '', '3.01', '2', '2021-02-01 20:36:11', 'aktif');

-- ----------------------------
-- Table structure for keuangan_meta
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_meta`;
CREATE TABLE `keuangan_meta` (
  `meta_id` int(2) NOT NULL AUTO_INCREMENT,
  `meta_name` varchar(100) NOT NULL COMMENT 'Nama Website',
  `meta_desc` text,
  `meta_keyword` text,
  `meta_author` varchar(100) DEFAULT NULL,
  `meta_developer` varchar(50) DEFAULT NULL,
  `meta_robots` varchar(50) DEFAULT NULL,
  `meta_googlebots` varchar(50) DEFAULT NULL,
  `meta_header` text,
  `meta_owner` varchar(50) DEFAULT NULL,
  `meta_tanggal` date DEFAULT NULL,
  `meta_link` varchar(100) DEFAULT NULL COMMENT 'Link SIMPEG',
  `meta_update` datetime NOT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_meta
-- ----------------------------
INSERT INTO `keuangan_meta` VALUES ('1', 'SIA | Sistem Informasi Keuangan', ' Sistem Informasi Keuangan', 'keuangan, manujeku, spp, jurnal, rekening', 'Mustaqim', 'Mustaqim', 'index, follow', 'index, follow', '<p><strong>MANUJEKULO</strong><br>\r\nKota Kudus</p>\r\n', '-', '2021-02-01', 'http://sia.manujeku', '2020-02-25 13:11:08');

-- ----------------------------
-- Table structure for keuangan_rapb_det
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_rapb_det`;
CREATE TABLE `keuangan_rapb_det` (
  `rapb_det_id` int(11) NOT NULL AUTO_INCREMENT,
  `rapb_head_id` int(11) DEFAULT NULL,
  `rapb_mster_reg` int(11) DEFAULT NULL,
  `rapb_det_unit` varchar(100) DEFAULT NULL,
  `rapb_det_jumlah` double(10,2) DEFAULT NULL,
  `rapb_det_biaya_satuan` int(11) DEFAULT NULL,
  `rapb_det_biaya_sub_total` varchar(100) DEFAULT NULL,
  `rapb_det_log` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rapb_det_id`),
  UNIQUE KEY `keuangan_rapb_det_rapb_det_id_IDX` (`rapb_det_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_rapb_det
-- ----------------------------

-- ----------------------------
-- Table structure for keuangan_rapb_head
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_rapb_head`;
CREATE TABLE `keuangan_rapb_head` (
  `rapb_id` int(11) NOT NULL AUTO_INCREMENT,
  `rapb_tahun` varchar(100) DEFAULT NULL,
  `rapb_name` varchar(255) DEFAULT NULL,
  `rapb_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rapb_id`),
  UNIQUE KEY `keuangan_rapb_head_rapb_id_IDX` (`rapb_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_rapb_head
-- ----------------------------

-- ----------------------------
-- Table structure for keuangan_unit
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_unit`;
CREATE TABLE `keuangan_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_nama` varchar(100) DEFAULT NULL,
  `unit_update` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `keuangan_unit_unit_id_IDX` (`unit_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_unit
-- ----------------------------

-- ----------------------------
-- Table structure for keuangan_users
-- ----------------------------
DROP TABLE IF EXISTS `keuangan_users`;
CREATE TABLE `keuangan_users` (
  `user_username` varchar(30) NOT NULL,
  `user_password` text NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `user_email` varchar(50) DEFAULT NULL,
  `user_avatar` varchar(100) DEFAULT NULL,
  `user_status` enum('Aktif','Tidak Aktif') NOT NULL DEFAULT 'Aktif',
  `user_level` enum('Admin','OPD','User') NOT NULL DEFAULT 'User',
  `user_update` datetime NOT NULL,
  PRIMARY KEY (`user_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keuangan_users
-- ----------------------------
INSERT INTO `keuangan_users` VALUES ('admin', '7712fcffc21bb7215f58035ab4506a5873c4af3c', 'ADMINISTRATOR SIMPEG', 'it@hotelhomkudus.com', 'Avatar_admin_1542355052.jpg', 'Aktif', 'Admin', '2020-12-28 17:04:18');

-- ----------------------------
-- View structure for v_master_reg
-- ----------------------------
DROP VIEW IF EXISTS `v_master_reg`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_master_reg` AS select *,kode_gabung(subtitusi) as urut from v_subtitusi_master_reg order by urut asc ;

-- ----------------------------
-- View structure for v_subtitusi_master_reg
-- ----------------------------
DROP VIEW IF EXISTS `v_subtitusi_master_reg`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_subtitusi_master_reg` AS select
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_id` as `masreg_id`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_kode` as `masreg_kode`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_nama` as `masreg_nama`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_index` as `masreg_index`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_sub` as `masreg_sub`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_level` as `masreg_level`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_update` as `masreg_update`,
    `manujeku_keuangan`.`keuangan_master_reg`.`masreg_status` as `masreg_status`,
    replace(masreg_kode, '.' , '')  as `subtitusi`
--     `subtitusi`(`manujeku_keuangan`.`keuangan_master_reg`.`masreg_kode`) as `subtitusi`
from
    `manujeku_keuangan`.`keuangan_master_reg` ;

-- ----------------------------
-- Procedure structure for insert_master_reg
-- ----------------------------
DROP PROCEDURE IF EXISTS `insert_master_reg`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_master_reg`(uraian varchar(255),nm_index varchar(100),sub varchar(100))
begin
	declare tot_row int;
	declare totsub_row int;
	declare mak_reg varchar(10);
	declare lenght_point int;
-- 	declare mak_sub varchar(10);
	declare new_reg char(100);
	select ifnull(count(*),0) into tot_row from keuangan_master_reg ;
	if tot_row=0 then
		insert INTO  keuangan_master_reg(masreg_kode,masreg_nama,masreg_index,masreg_sub,masreg_level,masreg_update,masreg_status) values
		('1',uraian,nm_index,'','1',now(),'aktif');
	else 
		if nm_index='head' then
				select max(masreg_kode)+1 into mak_reg from keuangan_master_reg where masreg_level=0;
				insert INTO  keuangan_master_reg(masreg_kode,masreg_nama,masreg_index,masreg_sub,masreg_level,masreg_update,masreg_status) values
				(mak_reg,uraian,nm_index,'','0',now(),'aktif');
		else 
				select count(masreg_kode) into totsub_row from keuangan_master_reg where masreg_kode like CONCAT(sub, '%');
				select (((CHAR_LENGTH(masreg_kode) - CHAR_LENGTH(REPLACE(masreg_kode, '.', ''))) / CHAR_LENGTH('.')+1)-1) into lenght_point from keuangan_master_reg where masreg_kode=sub;
				if totsub_row = 1 then 
					SET @query = 'insert INTO  keuangan_master_reg(masreg_kode,masreg_nama,masreg_index,masreg_sub,masreg_level,masreg_update,masreg_status) values ( ?, ?, ?, ?, ?, ?, ?)';
	    			set @v1 = concat(sub,'.01');
	    			set @v2 = uraian;
	    			set @v3 = '';
	    			set @v4 = sub;
	    			set @v5 = (lenght_point + 1);
	    			set @v6 = now();
	    			set @v7 = 'aktif';
					PREPARE stmt FROM @query;
	    			EXECUTE stmt using @v1,@v2,@v3,@v4,@v5,@v6,@v7;
	    		else 
	    			select if(reg<10,concat(sub,'.0',(reg+1)),concat(sub,(reg+1))) into new_reg from (
					select masreg_kode,max(convert(SUBSTRING_INDEX(masreg_kode,'.',-1),SIGNED)) as reg
					from keuangan_master_reg where masreg_kode like CONCAT( sub , '%') and masreg_kode !=sub) x ;
					SET @query = 'insert INTO  keuangan_master_reg(masreg_kode,masreg_nama,masreg_index,masreg_sub,masreg_level,masreg_update,masreg_status) values ( ?, ?, ?, ?, ?, ?, ?)';
	    			set @v1 = new_reg;
	    			set @v2 = uraian;
	    			set @v3 = '';
	    			set @v4 = sub;
	    			set @v5 = (lenght_point + 1);
	    			set @v6 = now();
	    			set @v7 = 'aktif';
					PREPARE stmt FROM @query;
	    			EXECUTE stmt using @v1,@v2,@v3,@v4,@v5,@v6,@v7;
				end if;		
		end if;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for kode_gabung
-- ----------------------------
DROP FUNCTION IF EXISTS `kode_gabung`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `kode_gabung`(kode_reg_tnp_titik varchar(100)) RETURNS varchar(100) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
begin
	declare mx_lv int;
-- 	declare xyz varchar(100);
	declare hasil varchar(100);
	declare len int;
	select  max(LENGTH(subtitusi)) into mx_lv from v_subtitusi_master_reg ;
-- 	set xyz = replace(kode_reg, '.' , '');
	set len = LENGTH(kode_reg_tnp_titik);
	set hasil = concat(kode_reg_tnp_titik,REPEAT('0', (mx_lv-len)));
	RETURN hasil;
END
;;
DELIMITER ;
