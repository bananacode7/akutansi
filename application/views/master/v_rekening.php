<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
             <div class="col-xl-4 col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="text-white">input Rekening</h5>
                    </div>
                    <div class="card-block">
                        <form class="form" method="post" name="formInput" id="formInput">
                            <div class="form-group form-info" id="sub_div">
                                <label>Kode reg</label>
                                <input class="form-control form-bg-default form-control-round " name="sub" id="sub" readonly />


                            </div>
                            <div class="form-group form-info">
                                <select class="form-control" name="index" id="index">
                                    <option value="">Pilih Kelompok</option>
                                    <option value="head">Head</option>
                                    <option value="sub">Sub</option>
                                </select>

                            </div>

                            <div class="form-group form-info">

                                <textarea placeholder="Uraian Rekening" class="form-control" rows="6" id="uraian" name="uraian" /></textarea>

                            </div>
                            <div class="form-group form-info">
                                <input type="hidden" id="status_input" name="status_input" value="" />
                                <select class="form-control" name="status" id="status">
                                    <option value="">Pilih Status</option>
                                    <option value="aktif">Aktif</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                            <div class="form-group form-success">
                                <button type="submit" class="btn btn-success btn-block"><i class="ti-save"></i> Save
                                </button>
                                <button type="button" id="reset" class="btn btn-secondary btn-block"><i class="ti-reload"></i> Reset
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="text-white">Data Rekening</h5>
                    </div>
                    <div class="card-block">
                        <table style="width:100%"  class="table table-sm table-bordered table-striped" id="tableData">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No</th>
                                    <th>Kode Reg</th>
                                    <th>Uraian</th>
                                    <th>status</th>
                                </tr>
                            </thead>


                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalReg" tabindex="-1" role="dialog" aria-labelledby="modalRegTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Daftar Reg</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <table style="width:100%" class="table table-sm table-bordered table-striped" id="ModaltableData">
        <thead>
            <tr>
                <th>#</th>
                <th>No</th>
                <th>Kode Reg</th>
                <th>Uraian</th>
                <th>status</th>
                
            </tr>
        </thead>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    function reload_table() {
        table.ajax.reload(null,false);
    }
    function reload_table_modal() {
        tableModal.ajax.reload(null,false);
    }
    function pilih_aksi(id){
       $.ajax({
        url : "<?=site_url('master/rekening/get_data')?>/"+id,
        type: "GET",
        success: function(data) {
            var hasil =JSON.parse(data);
            $('#status_input').val('update');
            $('#sub_div').show();

            if(hasil.masreg_index =='head'){
                $('#index').val(hasil.masreg_index);
            }else{
                $('#index').val("sub");
            }
            $('#sub').val(hasil.masreg_kode);
            $('#uraian').val(hasil.masreg_nama);
            $('#status').val(hasil.masreg_status);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("error", "Get Data Error !", "error");
        }
    });
   }
   function pilih_input(id){
       $.ajax({
        url : "<?=site_url('master/rekening/get_data')?>/"+id,
        type: "GET",
        success: function(data){
            var hasil =JSON.parse(data);
            $('#status_input').val('insert');
            $('#sub_div').show();
            $('#index').val("sub");
            $('#sub').val(hasil.masreg_kode);
            $('#modalReg').modal('toggle');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("error", "Get Data Error !", "error");
        }
    });
   }
   function reset(){
    $('#status_input').val('');
    $('#sub').val('');
    $('#index').val('');
    $('#uraian').val('');
    $('#status').val('');
    $('#sub_div').hide();
}
function hapusData(unit_id) {
    var id = unit_id;
    swal({
        title: "Anda Yakin?",
        text: "Data ini akan dihapus",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
      $.ajax({
        url : "<?=site_url('master/rekening/deletedata')?>/"+id,
        type: "POST",
        success: function(data) {
            swal("Success", "Hapus data berhasil!",'success', {
              buttons: false,
              timer: 2000});
            reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown) {
         swal("Error", "Gagal Hapus data!",'error',{
          buttons: false,
          timer: 2000});
     }
 });
  });
}
$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: { 
            index:{ required: true },
            uraian:{ required: true },
            status:{ required: true }
        },
        messages: { 
            index: { required :'Kelompok required' },
            uraian: { required :'Uraian required' },
            status: { required :'Status required' }
        },
        submitHandler: function (form) {
            dataString = $("#formInput").serialize();
            $.ajax({
                url: '<?=site_url('master/rekening/savedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    swal("Success", "Simpan data berhasil!", "success",{
                      buttons: false,
                      timer: 2000});
                    console.log(data);
                    reset();
                    reload_table();
                    reload_table_modal();
                },
                error: function() {
                  swal("Error", "Get Data Error !", "error",{
                      buttons: false,
                      timer: 2000});
                  
                  reset();
              }
          });
        }
    });
});
$(document).ready(function() {
    $('#sub_div').hide();
    $('#reset').click(function(){
        reset();
    });
    $('#tabReg').prop('disabled', true);
    $('#index').change(function(){
        var val = $(this).val();
        if (val=='head') {
            reset();
            $('#tabReg').prop('disabled', true);
        }else if (val=='sub') {
            $('#modalReg').modal('show');
            $('#tabReg').prop('disabled', false);
        }
        else{
            $('#tabReg').prop('disabled', false);
        }
    });
});
var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('master/rekening/data_list');?>",
            "type": "POST"
        },
        "columnDefs": [
        {
            "targets": [ 0 ],
            "orderable": false,
        },
        {
            "targets": [ 0 ],
            "className": "dt-center",
        }
        ],
    });
});
var tableModal;
$(document).ready(function() {
    tableModal = $('#ModaltableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('master/rekening/data_list_modal');?>",
            "type": "POST"
        },
        "columnDefs": [
        {
            "targets": [ 0 ],
            "orderable": false,
        },
        {
            "targets": [ 0 ],
            "className": "dt-center",
        }
        ],
    });
});
</script>