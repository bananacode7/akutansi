<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-xl-12 col-md-24">
                    <div class="card">
                        <div class="card-header bg-info">
                           <div class="row">
                             <div class="col-md-4">
                                <a href="javascript:;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalAdd">
                                  <i class="ti-plus"></i> Tambah
                              </a>
                          </div>
                          <div class="col-md-8 text-right">
                            <h5 class="text-white">Data Tahun Ajaran</h5>

                        </div>                                
                    </div>

                </div>
                <div class="card-block">
                    <table width="100%"  class="table table-sm table-bordered table-striped" id="tableData">
                        <thead>
                            <tr>
                                 <th width="10%">#</th>
                                <th width="8%">No</th>
                                <th>tahun</th>
                               
                            </tr>
                        </thead>


                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>

<script type="text/javascript">


    function reload_table() {
        table.ajax.reload(null,false);
    }

    var table;
    $(document).ready(function() {
        table = $('#tableData').DataTable({
            "responsive": true,
            "processing": false,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?=site_url('master/tahun/data_list');?>",
                "type": "POST"
            },
            "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "dt-center",
            }
            ],
        });
    });

    $(document).ready(function() {
        $( "#formInput" ).validate({
            rules: { 
                tahun_nama: { required: true }

            },
            messages: { 
                tahun: { required :'Nama tahun required' }

            },
            submitHandler: function (form) {
                dataString = $("#formInput").serialize();
                $.ajax({
                    url: '<?=site_url('master/tahun/savedata');?>',
                    type: "POST",
                    data: dataString,
                    success: function(data) {
                       swal("Success", "Simpan data berhasil!", "success",{
                          buttons: false,
                          timer: 2000});
                       $('#formModalAdd').modal('hide');
                       resetformInput();
                       reload_table();
                   },
                   error: function() {
                       swal("Error", "Get Data Error !", "error",{
                          buttons: false,
                          timer: 2000});
                       $('#formModalAdd').modal('hide');
                       resetformInput();
                   }
               });
            }
        });
    });

    function resetformInput() {
        $("#nama").val('');

        var MValid = $("#formInput");
        MValid.validate().resetForm();
        MValid.find(".error").removeClass("error");
        MValid.removeAttr('aria-describedby');
        MValid.removeAttr('aria-invalid');
    }

    function edit_data(id) {
        $('#formEdit')[0].reset();
        $.ajax({
            url : "<?=site_url('master/tahun/get_data/');?>"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                $('#id').val(data.thn_id);
                $('#edit_thn_nama').val(data.thn_nama);
                $('#formModalEdit').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    $(document).ready(function() {
        $( "#formEdit" ).validate({
            rules: { 
             tahun: { required: true }
         },
         messages: { 
           tahun: { required :'Nama tahun required' }
       },
       invalidHandler: function(event, validator) {
        KTUtil.scrollTop();
    },
    submitHandler: function (form) {
        dataString = $("#formEdit").serialize();
        $.ajax({
            url: '<?=site_url('master/tahun/updatedata');?>',
            type: "POST",
            data: dataString,
            success: function(data) {
               swal("Success", "Simpan data berhasil!",'success', {
                  buttons: false,
                  timer: 2000});
               $('#formModalEdit').modal('hide');
               reload_table();
           },
           error: function() {
             swal("Error", "Gagal Simpan data!",'error',{
              buttons: false,
              timer: 2000});
             $('#formModalEdit').modal('hide');
         }
     });
    }
});
    });

    function hapusData(tahun_id) {
        var id = tahun_id;
        swal({
            title: "Anda Yakin?",
            text: "Data ini akan dihapus",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
          $.ajax({
            url : "<?=site_url('master/tahun/deletedata')?>/"+id,
            type: "POST",
            success: function(data) {
                swal("Success", "Hapus data berhasil!",'success', {
                  buttons: false,
                  timer: 2000});
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
               swal("Error", "Gagal Hapus data!",'error',{
                  buttons: false,
                  timer: 2000});
           }
       });
      });
    }
</script>

<div class="modal fade" id="formModalAdd" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-add"></i> Form Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" method="post" name="formInput" id="formInput">
                <div class="modal-body">

                    <div class="form-group">
                        <label>tahun</label>
                        <input type="text" class="form-control" placeholder="Input" name="thn_nama" id="thn_nama" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="flaticon2-cancel-music"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="formModalEdit" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-edit"></i> Form Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" method="post" name="formEdit" id="formEdit">

                <div class="modal-body">

                  <div class="form-group">
                    <label>Tahun</label>
                    <input type="hidden" name="id" id="id">
                    <input type="text" class="form-control" placeholder="Input Tahun" name="thn_nama" id="edit_thn_nama" autocomplete="off">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="flaticon2-cancel-music"></i> Batal</button>
            </div>
        </form>
    </div>
</div>
</div>