<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-xl-12 col-md-24">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="row">
                               <div class="col-md-4">
                                <a href="javascript:;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalAdd">
                                  <i class="ti-plus"></i> Tambah
                              </a>
                          </div>
                          <div class="col-md-8 text-right">
                            <h5 class="text-white">Data Unit</h5>

                        </div>                                
                    </div>
                </div>
                <div class="card-block">
                    <table width="100%" class="table table-sm table-bordered table-striped" id="tableData">
                        <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th width="8%">No</th>
                                <th>Unit</th>

                            </tr>
                        </thead>


                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>

<script type="text/javascript">


   function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('master/unit/data_list');?>",
            "type": "POST"
        },
        "columnDefs": [
        {
            "targets": [ 0, 1 ],
            "orderable": false,
        },
        {
            "targets": [ 0, 1 ],
            "className": "dt-center",
        }
        ],
    });
});

$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: { 
            unit_nama: { required: true }
        },
        messages: { 
            unit_nama: { required :'Nama unit required' }

        },
        submitHandler: function (form) {
            dataString = $("#formInput").serialize();
            $.ajax({
                url: '<?=site_url('master/unit/savedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    swal("Success", "Simpan data berhasil!", "success",{
                      buttons: false,
                      timer: 2000});
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                    reload_table();
                },
                error: function() {
                  swal("Error", "Get Data Error !", "error",{
                      buttons: false,
                      timer: 2000});
                  $('#formModalAdd').modal('hide');
                  resetformInput();
              }
          });
        }
    });
});

function resetformInput() {
    $("#nama").val('');

    var MValid = $("#formInput");
    MValid.validate().resetForm();
    MValid.find(".error").removeClass("error");
    MValid.removeAttr('aria-describedby');
    MValid.removeAttr('aria-invalid');
}

function edit_data(id) {
    $('#formEdit')[0].reset();
    $.ajax({
        url : "<?=site_url('master/unit/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.unit_id);
            $('#unit').val(data.unit_nama);
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    $( "#formEdit" ).validate({
        rules: { 
           unit: { required: true }
       },
       messages: { 
         unit: { required :'Nama unit required' }
     },
     invalidHandler: function(event, validator) {
        KTUtil.scrollTop();
    },
    submitHandler: function (form) {
        dataString = $("#formEdit").serialize();
        $.ajax({
            url: '<?=site_url('master/unit/updatedata');?>',
            type: "POST",
            data: dataString,
            success: function(data) {
                swal("Success", "Simpan data berhasil!",'success', {
                  buttons: false,
                  timer: 2000});
                $('#formModalEdit').modal('hide');
                reload_table();
            },
            error: function() {
               swal("Error", "Gagal Simpan data!",'error',{
                  buttons: false,
                  timer: 2000});
               $('#formModalEdit').modal('hide');
           }
       });
    }
});
});

function hapusData(unit_id) {
    var id = unit_id;
    swal({
        title: "Anda Yakin?",
        text: "Data ini akan dihapus",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
      $.ajax({
        url : "<?=site_url('master/unit/deletedata')?>/"+id,
        type: "POST",
        success: function(data) {
            swal("Success", "Hapus data berhasil!",'success', {
              buttons: false,
              timer: 2000});
            reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown) {
           swal("Error", "Gagal Hapus data!",'error',{
              buttons: false,
              timer: 2000});
       }
   });
  });
}
</script>

<div class="modal fade" id="formModalAdd" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-add"></i>Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" method="post" name="formInput" id="formInput">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Unit</label>
                        <input type="text" class="form-control" placeholder="Input" 
                        name="unit_nama" id="unit_nama" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="flaticon2-cancel-music"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="formModalEdit" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-edit"></i> Form Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" method="post" name="formEdit" id="formEdit">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">

                  <div class="form-group">
                    <label>Unit</label>
                    <input type="text" class="form-control" placeholder="Input Unit" name="unit" id="unit" autocomplete="off">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="flaticon2-cancel-music"></i> Batal</button>
            </div>
        </form>
    </div>
</div>
</div>