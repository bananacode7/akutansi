<?php
$meta = $this->menu_m->select_meta()->row();
?>
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
	<div class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-between">
		<div class="text-dark order-2 order-md-1">
			<span class="text-muted font-weight-bold mr-2">2020 &copy;</span><?=$meta->meta_name;?>
		</div>
	</div>
</div>