<?php 
$uri          = $this->uri->segment(1);
$master_Data       = ['rekening','rapb','unit','tahun'];
$claster_menu = ['master' => $master_Data];
$var_arr      = ['master' => '','tahun' => '', 'rekening'    => '','unit' => '', 'rapb' => ''];

if ($uri != 'main') {
	$dashboard = '';
} else {
	$dashboard = 'active';
}
$var_arr[$uri] = 'active';
extract($var_arr);
foreach ($claster_menu as $menu => $sub_menu) {
	if (in_array($uri, $sub_menu)) {
		switch ($menu) {
			case 'master':
			$master = 'active pcoded-trigger';
			break;
			default:
                # code...
			break;
		}

		break;
	}
}
?>


<nav class="pcoded-navbar">
	<div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
	<div class="pcoded-inner-navbar main-menu">
		<div class="">
			<div class="main-menu-header">
				<img class="img-80 img-radius bg-dark" src="assets/images/user.png" alt="User-Profile-Image">
				<div class="user-details">
					<span id="more-details"><?=$uri?><i class="fa fa-caret-down"></i></span>
				</div>
			</div>
			<div class="main-menu-content">
				<ul>
					<li class="more-details">
						<a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
						<a href="#!"><i class="ti-settings"></i>Settings</a>
						<a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="pcoded-navigation-label">Navigation</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="<?=$dashboard?>">
				<a href="index.html" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
					<span class="pcoded-mtext">Dashboard</span>
					<span class="pcoded-mcaret"></span>
				</a>
			</li>
		</ul>
		<div class="pcoded-navigation-label">Data Utama</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu <?=$master?>">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>BC</b></span>
					<span class="pcoded-mtext">Master</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="<?=$unit?>">
						<a href="<?=site_url('unit')?>" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Unit</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="<?=$tahun?>">
						<a href="<?=site_url('tahun')?>" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Tahun Ajaran</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="<?=$rekening?>">
						<a href="<?= site_url('rekening')?>" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Rekening</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="<?=$rapb?>">
						<a href="<?= site_url('master/rapb')?>" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">RAPB</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					
				</ul>
			</li>
		</ul>
		<div class="pcoded-navigation-label">Forms</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="">
				<a href="form-elements-component.html" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
					<span class="pcoded-mtext">Form</span>
					<span class="pcoded-mcaret"></span>
				</a>
			</li>
		</ul>
		<div class="pcoded-navigation-label">Tables</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="">
				<a href="bs-basic-table.html" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-receipt"></i><b>B</b></span>
					<span class="pcoded-mtext">Table</span>
					<span class="pcoded-mcaret"></span>
				</a>
			</li>
		</ul>
		<div class="pcoded-navigation-label">Chart And Maps</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="">
				<a href="chart-morris.html" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i><b>C</b></span>
					<span class="pcoded-mtext">Charts</span>
					<span class="pcoded-mcaret"></span>
				</a>
			</li>
			<li class="">
				<a href="map-google.html" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-map-alt"></i><b>M</b></span>
					<span class="pcoded-mtext">Maps</span>
					<span class="pcoded-mcaret"></span>
				</a>
			</li>
		</ul>
		<div class="pcoded-navigation-label">Pages</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu ">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
					<span class="pcoded-mtext">Pages</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="">
						<a href="auth-normal-sign-in.html" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Login</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="">
						<a href="auth-sign-up.html" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Registration</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="">
						<a href="sample-page.html" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="ti-layout-sidebar-left"></i><b>S</b></span>
							<span class="pcoded-mtext">Sample Page</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>