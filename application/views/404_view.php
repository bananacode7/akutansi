<?php
$meta     = $this->menu_m->select_meta()->row();
?>
<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title><?=$meta->meta_name;?></title>
    <link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <link href="<?=base_url('backend/assets/css/pages/error/error-5.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('backend/assets/plugins/global/plugins.bundle.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('backend/assets/plugins/custom/prismjs/prismjs.bundle.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('backend/assets/css/style.bundle.css');?>" rel="stylesheet" type="text/css"/>
    <script src="<?=base_url('backend/assets/plugins/global/plugins.bundle.js');?>"></script>
</head>
<body  id="kt_body"  class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading"  >
    <div class="d-flex flex-column flex-root">
        <div class="error error-5 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url(<?=base_url('img/bg5.jpg');?>);">
            <div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12">
                <h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">Oops!</h1>
                <p class="font-weight-boldest display-4">404 Not Found</p>
                <p class="font-size-h3">Halaman ini tidak ditemukan</p>
            </div>
        </div>
    </div>

    <script>
    var KTAppSettings = {
        "breakpoints": {
            "sm": 576,
            "md": 768,
            "lg": 992,
            "xl": 1200,
            "xxl": 1400
        },
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#E4E6EF",
                    "dark": "#181C32"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#EBEDF3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#3F4254",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#EBEDF3",
                "gray-300": "#E4E6EF",
                "gray-400": "#D1D3E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#7E8299",
                "gray-700": "#5E6278",
                "gray-800": "#3F4254",
                "gray-900": "#181C32"
            }
        },
        "font-family": "Poppins"
    };
    </script>
    <script src="<?=base_url('backend/assets/plugins/custom/prismjs/prismjs.bundle.js');?>"></script>
    <script src="<?=base_url('backend/assets/js/scripts.bundle.js');?>"></script>
    </body>
</html>