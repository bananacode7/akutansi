<?php
$meta = $this->menu_m->select_meta()->row();
?>
<div class="footer bg-white py-4 d-flex flex-lg-column " id="kt_footer">
	<div class=" container  d-flex flex-column flex-md-row align-items-center justify-content-between">
		<div class="text-dark order-2 order-md-1">
			<span class="text-muted font-weight-bold mr-2">2020 &copy;</span><?=$meta->meta_name;?>
		</div>
		<div class="nav nav-dark order-1 order-md-2">
			<a href="<?=site_url('kontak-kami');?>" class="nav-link pr-3 pl-0">Kontak Kami</a>
		</div>
	</div>
</div>