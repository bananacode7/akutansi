<?php
$uri = $this->uri->segment(1);
if ($uri == 'wajib-pajak') {
    $wajibpajak = 'menu-item-active';
    $objekpajak = '';
    $reklame    = '';
    $bphtb      = '';
    $pbb        = '';
    $realisasi  = '';
} elseif ($uri == 'objek-pajak') {
    $wajibpajak = '';
    $objekpajak = 'menu-item-active';
    $reklame    = '';
    $bphtb      = '';
    $pbb        = '';
    $realisasi  = '';
} elseif ($uri == 'reklame') {
    $wajibpajak = '';
    $objekpajak = '';
    $reklame    = 'menu-item-active';
    $bphtb      = '';
    $pbb        = '';
    $realisasi  = '';
} elseif ($uri == 'bphtb') {
    $wajibpajak = '';
    $objekpajak = '';
    $reklame    = '';
    $bphtb      = 'menu-item-active';
    $pbb        = '';
    $realisasi  = '';
} elseif ($uri == 'pbb') {
    $wajibpajak = '';
    $objekpajak = '';
    $reklame    = '';
    $bphtb      = '';
    $pbb        = 'menu-item-active';
    $realisasi  = '';
} elseif ($uri == 'realisasi-pajak') {
    $wajibpajak = '';
    $objekpajak = '';
    $reklame    = '';
    $bphtb      = '';
    $pbb        = '';
    $realisasi  = 'menu-item-active';
} else {
    $wajibpajak = '';
    $objekpajak = '';
    $reklame    = '';
    $bphtb      = '';
    $pbb        = '';
    $realisasi  = '';
}
?>

<div id="kt_header" class="header header-fixed" >
    <?php
    if ($this->uri->segment(1) != '') {
    ?>
    <div class="container d-flex align-items-stretch justify-content-between">
        <div class="d-flex align-items-stretch mr-3">
            <div class="header-logo">
                <a href="<?=base_url();?>">
                    <img alt="Logo" src="<?=base_url('img/logo-depan.png');?>" class="logo-default max-h-45px"/>
                    <img alt="Logo" src="<?=base_url('img/logo-depan.png');?>" class="logo-sticky max-h-45px"/>
                </a>
            </div>
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile  header-menu-layout-default " >
                    <ul class="menu-nav ">
                        <li class="menu-item <?=$wajibpajak;?>">
                            <a href="<?=site_url('wajib-pajak');?>" class="menu-link"><span class="menu-text">Wajib Pajak</span></a>
                        </li>
                        <li class="menu-item <?=$objekpajak;?>">
                            <a href="<?=site_url('objek-pajak');?>" class="menu-link"><span class="menu-text">Objek Pajak</span></a>
                        </li>
                        <li class="menu-item <?=$reklame;?>">
                            <a href="<?=site_url('reklame');?>" class="menu-link"><span class="menu-text">Reklame</span></a>
                        </li>
                        <li class="menu-item <?=$bphtb;?>">
                            <a href="<?=site_url('bphtb');?>" class="menu-link"><span class="menu-text">BPHTB</span></a>
                        </li>
                        <li class="menu-item <?=$pbb;?>">
                            <a href="<?=site_url('pbb');?>" class="menu-link"><span class="menu-text">PBB</span></a>
                        </li>
                        <li class="menu-item <?=$realisasi;?>">
                            <a href="<?=site_url('realisasi-pajak');?>" class="menu-link"><span class="menu-text">Realisasi Pajak</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
</div>