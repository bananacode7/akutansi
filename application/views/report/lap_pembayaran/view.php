<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Laporan-Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Pembayaran</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon-list-1 text-primary"></i></span>
                        Laporan Pembayaran
                    </h3>
                </div>

                <form class="form" id="formInput" name="formInput" method="post">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Pasar</label>
                            <div class="col-4">
                                <select class="form-control" name="lstPasar" id="lstPasar">
                                    <option value="">- Pilih Pasar -</option>
                                    <?php foreach($listPasar as $r) { ?>
                                    <option value="<?=$r->pasar_id;?>"><?=$r->pasar_nama;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Periode</label>
                            <div class="col-4">
                                <div class="input-daterange input-group">
                                    <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" autocomplete="off" value="01-01-<?=date('Y');?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                    </div>
                                    <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" autocomplete="off" value="<?=date('d-m-Y');?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Tempat</label>
                            <div class="col-4">
                                <select class="form-control" name="lstTempat" id="lstTempat">
                                    <option value="">- SEMUA -</option>
                                    <?php foreach($listTempat as $r) { ?>
                                    <option value="<?=$r->tempat_id;?>"><?=$r->tempat_nama;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Tipe Bayar</label>
                            <div class="col-4">
                                <select class="form-control" name="lstTipe" id="lstTipe">
                                    <option value="">- SEMUA -</option>
                                    <option value="C">TUNAI</option>
                                    <option value="E">E-RETRIBUSI</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-9">
                                <a href="javascript:;" class="btn btn-primary" id="btn-print">
                                    <i class="fa fa-print"></i><span class="kt-hidden-mobile">Print</span>
                                </a>
                                <a href="javascript:;" class="btn btn-primary" id="btn-printrekap">
                                    <i class="fa fa-print"></i><span class="kt-hidden-mobile">Rekap Item</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    $('.date-picker').datepicker({
        rtl: KTUtil.isRTL(),
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows
    });
});

$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: { 
            lstPasar: { required: true }
        },
        messages: { 
            lstPasar: { required :'Pasar required' }
        }
    });

    $("#btn-print").click(function() {
        if($('#formInput').valid()) {
            printData();
        }
    });

    $("#btn-printrekap").click(function() {
        if($('#formInput').valid()) {
            printDataRekap();
        }
    });
});

function printData() {
    var pasar      = $('#lstPasar').val();
    var tgl_dari   = $('#tgl_dari').val();
    var tgl_sampai = $('#tgl_sampai').val();
    var lstTipe    = $('#lstTipe').val();
    if (lstTipe == '') {
        var tipe = 'all';
    } else {
        var tipe = lstTipe;
    }
    var lstTempat = $('#lstTempat').val();
    if (lstTempat == '') {
        var tempat = 'all';
    } else {
        var tempat = lstTempat;
    }
    var url = "<?=site_url('report/lap_pembayaran/preview/');?>"+pasar+'/'+tgl_dari+'/'+tgl_sampai+'/'+tipe+'/'+tempat;
    window.open(url, "_blank");
}

function printDataRekap() {
    var pasar      = $('#lstPasar').val();
    var tgl_dari   = $('#tgl_dari').val();
    var tgl_sampai = $('#tgl_sampai').val();
    var lstTipe    = $('#lstTipe').val();
    if (lstTipe == '') {
        var tipe = 'all';
    } else {
        var tipe = lstTipe;
    }
    var lstTempat = $('#lstTempat').val();
    if (lstTempat == '') {
        var tempat = 'all';
    } else {
        var tempat = lstTempat;
    }
    var url = "<?=site_url('report/lap_pembayaran/previewrekap/');?>"+pasar+'/'+tgl_dari+'/'+tgl_sampai+'/'+tipe+'/'+tempat;
    window.open(url, "_blank");
}
</script>