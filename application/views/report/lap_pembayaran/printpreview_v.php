<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Laporan Pembayaran</title>
<style type="text/css">
    table {
        border: 1px solid #ccccb3;
        width: 100%;
        border-collapse: collapse;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
        color: black;
        padding: 2px;
        border: 1px solid black;
        text-transform: uppercase;
    }

    td {
        padding: 3px;
        border: 1px solid black;
        vertical-align: top;
    }

    .page {
        width: 21cm;
        min-height: 29.4cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    body{
        font-family: "Franklin Gothic Medium"; 
        font-size:11px
    }

    h1{
        font-size:15px
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <div align="center" style="font-size: 15px;">LAPORAN PEMBAYARAN</div>
    <div align="center" style="font-size: 15px;"><?=$pasar;?></div>
    <div align="center" style="font-size: 12px;">PERIODE : <?=$this->uri->segment(5).' s/d '.$this->uri->segment(6);?></div>
    <br>
    <table align="center" width="100%">
        <tr>
            <th width="5%">No</th>
            <th width="10%">No. SKRD</th>
            <th width="10%">No. NPWRD</th>
            <th width="25%">Nama Pedagang</th>
            <th>Pasar</th>
            <th width="10%">Tgl. Bayar</th>
            <th width="10%">Tipe</th>
            <th width="10%">Total</th>
        </tr>
        <?php
        $pasar_id   = $this->uri->segment(4); 
        $tgl_dari   = date('Y-m-d', strtotime($this->uri->segment(5))); 
        $tgl_sampai = date('Y-m-d', strtotime($this->uri->segment(6))); 
        $tipe       = $this->uri->segment(7); 
        $totalskrd  = 0;
        foreach ($listData as $r) {
            $tempat_id  = $r->tempat_id;
            if ($tipe != 'all') {
                $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_skrd', array('tempat_id' => $tempat_id, 'skrd_tipe_bayar' => $tipe, 'pasar_id' => $pasar_id, 'skrd_tgl_bayar >=' => $tgl_dari, 'skrd_tgl_bayar <=' => $tgl_sampai, 'skrd_status' => 2))->result();
            } else {
                $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_skrd', array('tempat_id' => $tempat_id, 'pasar_id' => $pasar_id, 'skrd_tgl_bayar >=' => $tgl_dari, 'skrd_tgl_bayar <=' => $tgl_sampai, 'skrd_status' => 2))->result();
            }

            if (count($listDetail) > 0) {
        ?>
        <tr>
            <th width="100%" colspan="8"><?=$r->tempat_nama;?></th>
        </tr>
        <?php 
        $no       = 1;
        $subtotal = 0;
        foreach($listDetail as $d) {
            $total = ($d->skrd_total+$d->skrd_bunga+$d->skrd_kenaikan);
        ?>
        <tr>
            <td align="center" valign="top"><?=$no;?></td>
            <td valign="top" align="center"><?=$d->skrd_no;?></td>
            <td valign="top" align="center"><?=$d->dasar_npwrd;?></td>
            <td valign="top"><?=$d->penduduk_nik.'<br>'.$d->penduduk_nama;?></td>
            <td valign="top"><?=$d->pasar_nama.'<br>BLOK '.$d->dasar_blok.' NOMOR '.$d->dasar_nomor; ?></td>
            <td valign="top" align="center"><?=date('d-m-Y', strtotime($d->skrd_tgl_bayar));?></td>
            <td valign="top" align="center"><?=($d->skrd_tipe_bayar=='E'?'E-RETRIBUSI':'TUNAI');?></td>
            <td valign="top" align="right"><?=number_format($total,0,'',',');?></td>
        </tr>
        <?php
                $subtotal  = ($subtotal+$total);
                $totalskrd = ($totalskrd+$total);
                $no++;
            }
        ?>
        <tr>
            <td colspan="7" align="center">SUB TOTAL</td>
            <td align="right"><?=number_format($subtotal,0,'',',');?></td>
        </tr>
        <?php
            }
        }
        ?>
        <tr>
            <td colspan="7" align="center">TOTAL</td>
            <td align="right"><?=number_format($totalskrd,0,'',',');?></td>
        </tr>
    </table>
</div>
</body>
</html>