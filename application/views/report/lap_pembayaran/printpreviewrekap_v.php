<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Laporan Pembayaran Rekap</title>
<style type="text/css">
    table {
        border: 1px solid #ccccb3;
        width: 100%;
        border-collapse: collapse;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
        color: black;
        padding: 2px;
        border: 1px solid black;
        text-transform: uppercase;
    }

    td {
        padding: 3px;
        border: 1px solid black;
        vertical-align: top;
    }

    .page {
        width: 21cm;
        min-height: 29.4cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    body{
        font-family: "Franklin Gothic Medium"; 
        font-size:11px
    }

    h1{
        font-size:15px
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <div align="center" style="font-size: 15px;">LAPORAN REKAP PEMBAYARAN RETRIBUSI</div>
    <div align="center" style="font-size: 15px;"><?=$pasar;?></div>
    <div align="center" style="font-size: 12px;">PERIODE : <?=$this->uri->segment(5).' s/d '.$this->uri->segment(6);?></div>
    <br>
    <?php 
    for($i=0;$i<2;$i++) { 
        if ($i == 0) {
            $tipe       = 'C';
            $namatipe   = 'TUNAI';
        } else {
            $tipe       = 'E';
            $namatipe   = 'E-RETRIBUSI';
        }
    ?>
    <table align="center" width="100%">
        <tr>
            <th width="5%">No</th>
            <th width="15%">Kode Rekening</th>
            <th>Uraian</th>
            <th width="15%">Sub Total</th>
        </tr>
        <?php
        $pasar_id   = $this->uri->segment(4); 
        $tgl_dari   = date('Y-m-d', strtotime($this->uri->segment(5))); 
        $tgl_sampai = date('Y-m-d', strtotime($this->uri->segment(6))); 
        $totalskrd  = 0;
        foreach ($listData as $r) {
            $tempat_id  = $r->tempat_id;
            $Kd_Rek     = $r->tempat_kd_rek;
        ?>
        <tr>
            <th width="100%" colspan="4"><?=$r->tempat_nama;?></th>
        </tr>
        <?php
        $no         = 1;
        $subtotal   = 0;
        foreach ($listKomponen as $k) {
            $komponen_id = $k->komponen_id;
            if ($k->komponen_id == 1) {
                $komponen_kode = trim($k->komponen_kode . $Kd_Rek);
            } else {
                $komponen_kode = trim($k->komponen_kode);
            }

            $dataSub  = $this->db->select_sum('item_subtotal', 'subtotal')->get_where('v_skrd_item', array('pasar_id' => $pasar_id, 'tempat_id' => $tempat_id, 'skrd_tipe_bayar' => $tipe, 'skrd_tgl_bayar >=' => $tgl_dari, 'skrd_tgl_bayar <=' => $tgl_sampai, 'skrd_status' => 2, 'komponen_id' => $komponen_id))->row();
        ?>
        <tr>
            <td align="center" valign="top"><?=$no;?></td>
            <td valign="top"><?=$komponen_kode;?></td>
            <td valign="top"><?=$k->komponen_uraian;?></td>
            <td align="right" valign="top"><?=number_format($dataSub->subtotal,0,'',',');?></td>
        </tr>
        <?php
            $totalskrd = ($totalskrd+$dataSub->subtotal);
            $no++;
            }
        }
        ?>
        <tr>
            <td colspan="3" align="center"><b>TOTAL <?=$namatipe;?></b></td>
            <td align="right"><b><?=number_format($totalskrd,0,'',',');?></b></td>
        </tr>
    </table>
    <br>
    <?php } ?>
</div>
</body>
</html>