<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Laporan Transaksi Pedagang</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
        color: black;
        padding: 2px;
        text-transform: uppercase;
    }

    td {
        padding: 3px;
        vertical-align: top;
    }

    .page {
        width: 21cm;
        min-height: 29.4cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    body{
        font-family: "Franklin Gothic Medium"; 
        font-size:11px
    }

    h1{
        font-size:15px
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <div align="center" style="font-size: 15px;">LAPORAN TRANSAKSI PEDAGANG</div>
    <br>
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td width="13%">N P W R D</td>
            <td width="2%">:</td>
            <td width="85%"><?=$detail->dasar_npwrd;?></td>
        </tr>
        <tr>
            <td>No. Surat</td>
            <td>:</td>
            <td><?=$detail->dasar_no;?></td>
        </tr>
        <tr>
            <td>N I K</td>
            <td>:</td>
            <td><?=$detail->penduduk_nik;?></td>
        </tr>
        <tr>
            <td>Nama Pedagang</td>
            <td>:</td>
            <td><?=$detail->penduduk_nama;?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?=$detail->penduduk_alamat.' DESA '.$detail->desa_nama.' KEC. '.$detail->kecamatan_nama.' '.$detail->kabupaten_nama.' '.$detail->provinsi_nama;?></td>
        </tr>
        <tr>
            <td>Pasar</td>
            <td>:</td>
            <td><?=$detail->pasar_nama.' BLOK '.$detail->dasar_blok.' NO. '.$detail->dasar_nomor;?></td>
        </tr>
    </table>
    <br>
    <table cellpadding="2" cellspacing="2" border="1">
        <tr>
            <th width="5%">NO</th>
            <th width="30%">NO. SKRD</th>
            <th width="10%">TANGGAL</th>
            <th width="15%">PERIODE</th>
            <th width="10%">TGL. BAYAR</th>
            <th width="15%">TIPE BAYAR</th>
            <th width="15%">TOTAL</th>
        </tr>
        <?php 
        $no = 1;
        foreach($listData as $r) {
        ?>
        <tr>
            <td align="center"><?=$no;?></td>
            <td><?=$r->skrd_no;?></td>
            <td align="center"><?=date('d-m-Y', strtotime($r->skrd_tgl));?></td>
            <td align="center"><?=getBulan($r->skrd_bulan) . ' ' . $r->skrd_tahun;?></td>
            <td align="center"><?=($r->skrd_tgl_bayar != '' ? date('d-m-Y', strtotime($r->skrd_tgl_bayar)) :'BELUM BAYAR');?></td>
            <td align="center"><?=($r->skrd_tgl_bayar != '' ?($r->skrd_tipe_bayar == 'E' ? 'E-RETRIBUSI' : 'TUNAI'):'-');?></td>
            <td align="right"><?=number_format($r->skrd_total + $r->skrd_bunga + $r->skrd_kenaikan, 0, '', ',');?></td>
        </tr>
        <?php 
            $no++;
        }
        ?>
    </table>
</div>
</body>
</html>