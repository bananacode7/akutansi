<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Laporan-Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('report/lap_detailpedagang');?>" class="text-muted">Detail Pedagang</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">History Transaksi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="container ">
                    <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <span class="card-icon"><i class="flaticon2-edit text-primary"></i></span>
                                Detail Transaksi SKRD
                            </h3>
                            <div class="card-toolbar">
                                <a href="<?=site_url('report/lap_detailpedagang');?>" class="btn btn-clean">
                                    <i class="la la-arrow-left"></i>
                                    <span class="kt-hidden-mobile">Batal</span>
                                </a>
                            </div>
                        </div>

                        <div class="card-body">
                            <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Pendasaran</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row" width="20%">No. NPWRD</th>
                                                <td width="2%">:</td>
                                                <td width="28%"><?=$detail->dasar_npwrd;?></td>
                                                <th scope="row">No. Surat</th>
                                                <td>:</td>
                                                <td><?=$detail->dasar_no;?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Tgl. Habis</th>
                                                <td>:</td>
                                                <td><?=date('d-m-Y', strtotime($detail->dasar_sampai));?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">N I K</th>
                                                <td>:</td>
                                                <td><?=$detail->penduduk_nik;?></td>
                                                <th scope="row">Nama Pedagang</th>
                                                <td>:</td>
                                                <td><?=$detail->penduduk_nama;?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Alamat</th>
                                                <td>:</td>
                                                <td colspan="4"><?=$detail->penduduk_alamat.' DESA '.$detail->desa_nama.' KEC. '.$detail->kecamatan_nama.' '.$detail->kabupaten_nama.' '.$detail->provinsi_nama;?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Pasar</th>
                                                <td>:</td>
                                                <td colspan="4"><?=$detail->pasar_nama.' BLOK '.$detail->dasar_blok.' NO. '.$detail->dasar_nomor;?></td>
                                            </tr>
                                        </tbody>
                                    </table>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon"><i class="flaticon2-list-1 text-primary"></i></span>
                                <h3 class="card-label">Daftar Transaksi</h3>
                            </div>
                            
                            <div class="card-toolbar">
                                <a href="<?=site_url('report/lap_detailpedagang/printdata/'.$detail->dasar_id);?>" class="btn btn-clean" target="_blank">
                                    <i class="flaticon-technology"></i> Print Transaksi
                                </a>
                            </div>
                        </div>

                        <div class="card-body">
                            <table class="table table-bordered table-hover" id="tableData">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>No. SKRD</th>
                                        <th width="10%">Tanggal</th>
                                        <th width="15%">Periode</th>
                                        <th width="10%">Tgl. Bayar</th>
                                        <th width="15%">Tipe Bayar</th>
                                        <th width="15%">Total</th>
                                    </tr>
                                </thead>
                           </table>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    var dasar_id = '<?=$detail->dasar_id;?>';
    table = $('#tableData').DataTable({
        "info": false,
        "paging": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('report/lap_detailpedagang/data_detail_list/');?>"+dasar_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 2, 3, 4, 5 ],
                "className": "dt-center",
            },
            {
                "targets": [ 6 ],
                "className": "dt-right",
            }
        ],
    });
});
</script>
