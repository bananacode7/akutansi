<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Laporan-Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Detail Pedagang</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon"><i class="flaticon2-list-1 text-primary"></i></span>
                        <h3 class="card-label">Daftar Pedagang</h3>
                    </div>
                    
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                            <i class="fas fa-filter"></i> Filter Data
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-hover" id="tableData">
                        <thead>
                            <tr>
                                <th width="5%"></th>
                                <th width="5%">No</th>
                                <th width="15%">No. Surat</th>
                                <th width="10%">Tgl. Habis</th>
                                <th width="10%">NPWRD</th>
                                <th>Nama Pedagang</th>
                                <th width="20%">Pasar</th>
                                <th width="10%">Status</th>
                            </tr>
                        </thead>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    $('.date-picker').datepicker({
        rtl: KTUtil.isRTL(),
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows
    });
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [15, 30, 50, 75, 100, -1],
                [15, 30, 50, 75, 100, "All"]
        ],
        "pageLength": 15,
        "ajax": {
            "url": "<?=site_url('report/lap_detailpedagang/data_list');?>",
            "type": "POST",
            "data": function(data) {
                data.lstPasar         = $('#lstPasar').val();
                data.tgl_dari         = $('#tgl_dari').val();
                data.tgl_sampai       = $('#tgl_sampai').val();
                data.lstTempat        = $('#lstTempat').val();
                data.lstStatusDasar   = $('#lstStatusDasar').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 7 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3, 7 ],
                "className": "dt-center",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fas fa-filter"></i> Filter Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Pasar</label>
                        <select class="form-control" name="lstPasar" id="lstPasar">
                            <option value="">- SEMUA -</option>
                            <?php foreach($listPasar as $r) { ?>
                            <option value="<?=$r->pasar_id;?>"><?=$r->pasar_nama;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Periode Habis</label>
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" placeholder="DD-MM-YYYY" autocomplete="off"/>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                            </div>
                            <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" placeholder="DD-MM-YYYY" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tempat</label>
                        <select class="form-control" name="lstTempat" id="lstTempat">
                            <option value="">- SEMUA -</option>
                            <?php foreach($listTempat as $r) { ?>
                            <option value="<?=$r->tempat_id;?>"><?=$r->tempat_nama;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status Pendasaran</label>
                        <select class="form-control" name="lstStatusDasar" id="lstStatusDasar">
                            <option value="">- SEMUA -</option>
                            <option value="Baru">Baru</option>
                            <option value="Perpanjangan">Perpanjangan</option>
                            <option value="Balik Nama">Balik Nama</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>