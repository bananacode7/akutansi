<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Laporan Retribusi Detail</title>
<style type="text/css">
    table {
        border: 1px solid #ccccb3;
        width: 100%;
        border-collapse: collapse;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
        color: black;
        padding: 2px;
        border: 1px solid black;
        text-transform: uppercase;
    }

    td {
        padding: 3px;
        border: 1px solid black;
        vertical-align: top;
    }

    .page {
        width: 21cm;
        min-height: 29.4cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    body{
        font-family: "Franklin Gothic Medium"; 
        font-size:11px
    }

    h1{
        font-size:15px
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>

<?php 
if ($this->uri->segment(5) != 'all') {
    $periode = 'PERIODE : '.getBulan($this->uri->segment(5)).' '.$this->uri->segment(6);
} else {
    $periode = 'PERIODE : '.$this->uri->segment(6);
}
?>
<div class="page">
    <div align="center" style="font-size: 15px;">LAPORAN RETRIBUSI DETAIL</div>
    <div align="center" style="font-size: 15px;"><?=$pasar;?></div>
    <div align="center" style="font-size: 12px;"><?=$periode;?></div>
    <br>
    <table align="center" width="100%">
        <tr>
            <th width="5%">No</th>
            <th width="10%">No. SKRD</th>
            <th width="10%">No. NPWRD</th>
            <th width="25%">Nama Pedagang</th>
            <th>Pasar</th>
            <th width="10%">Tgl. Bayar</th>
            <th width="10%">Total</th>
        </tr>
        <?php
        $pasar_id   = $this->uri->segment(4); 
        $bulan      = $this->uri->segment(5);
        $tahun      = $this->uri->segment(6); 
        $status     = $this->uri->segment(7); 
        $totalskrd  = 0;
        foreach ($listData as $r) {
            $tempat_id  = $r->tempat_id;
            if ($status != 'all') {
                if ($bulan != 'all') {
                    $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_skrd', array('tempat_id' => $tempat_id, 'skrd_status' => $status, 'pasar_id' => $pasar_id, 'skrd_bulan' => $bulan, 'skrd_tahun' => $tahun))->result();
                } else {
                    $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_skrd', array('tempat_id' => $tempat_id, 'skrd_status' => $status, 'pasar_id' => $pasar_id, 'skrd_tahun' => $tahun))->result();
                }
            } else {
                if ($bulan != 'all') {
                    $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_skrd', array('tempat_id' => $tempat_id, 'pasar_id' => $pasar_id, 'skrd_bulan' => $bulan, 'skrd_tahun' => $tahun))->result();
                } else {
                    $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_skrd', array('tempat_id' => $tempat_id, 'pasar_id' => $pasar_id, 'skrd_tahun' => $tahun))->result();
                }
            }

            if (count($listDetail) > 0) {
        ?>
        <tr>
            <th width="100%" colspan="7"><?=$r->tempat_nama;?></th>
        </tr>
        <?php 
        $no       = 1;
        $subtotal = 0;
        foreach($listDetail as $d) {
            $skrd_id = $d->skrd_id;
            $total    = ($d->skrd_total+$d->skrd_bunga+$d->skrd_kenaikan);
        ?>
        <tr>
            <td align="center" valign="top"><?=$no;?></td>
            <td valign="top" align="center"><?=$d->skrd_no;?></td>
            <td valign="top" align="center"><?=$d->dasar_npwrd;?></td>
            <td valign="top"><?=$d->penduduk_nik.'<br>'.$d->penduduk_nama;?></td>
            <td valign="top"><?=$d->pasar_nama.'<br>BLOK '.$d->dasar_blok.' NOMOR '.$d->dasar_nomor; ?></td>
            <td valign="top" align="center"><?=($d->skrd_tgl_bayar!=''?date('d-m-Y', strtotime($d->skrd_tgl_bayar)):'BELUM BAYAR');?></td>
            <td valign="top" align="right"><?=number_format($total,0,'',',');?></td>
        </tr>
        <tr>
            <td colspan="7">
                <table cellpadding="2" cellspacing="2">
                    <tr>
                        <th width="10%">Kode Rekening</th>
                        <th width="20%">Uraian Retribusi</th>
                        <th width="10%">Luas</th>
                        <th width="10%">Tarif/Hari</th>
                        <th width="10%">Jml. Hari</th>
                        <th width="13%">Sub Total</th>
                    </tr>
                    <?php 
                    $listRinci = $this->db->order_by('item_id', 'asc')->get_where('sipp_skrd_item', array('skrd_id' => $skrd_id))->result();
                    foreach($listRinci as $x) {
                    ?>
                    <tr>
                        <td><?php echo $x->item_kode; ?></td>
                        <td><?php echo $x->item_uraian; ?></td>
                        <td align="right"><?php echo $x->item_luas.' '.$x->item_satuan; ?></td>
                        <td align="right"><?php echo $x->item_tarif; ?></td>
                        <td align="right"><?php echo $x->item_hari; ?></td>
                        <td align="right"><?php echo number_format($x->item_subtotal); ?></td>
                    </tr>
                    <?php
                    } 
                    ?>
                </table>
            </td>
        </tr>
        <?php
                $subtotal  = ($subtotal+$total);
                $totalskrd = ($totalskrd+$total);
                $no++;
            }
        ?>
        <tr>
            <td colspan="6" align="center">SUB TOTAL</td>
            <td align="right"><?=number_format($subtotal,0,'',',');?></td>
        </tr>
        <?php
            }
        }
        ?>
        <tr>
            <td colspan="6" align="center">TOTAL</td>
            <td align="right"><?=number_format($totalskrd,0,'',',');?></td>
        </tr>
    </table>
</div>
</body>
</html>