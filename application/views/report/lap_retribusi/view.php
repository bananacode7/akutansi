<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Laporan-Laporan</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Retribusi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon-list-1 text-primary"></i></span>
                        Laporan Retribusi
                    </h3>
                </div>

                <form class="form" id="formInput" name="formInput" method="post">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Pasar</label>
                            <div class="col-4">
                                <select class="form-control" name="lstPasar" id="lstPasar">
                                    <option value="">- Pilih Pasar -</option>
                                    <?php foreach($listPasar as $r) { ?>
                                    <option value="<?=$r->pasar_id;?>"><?=$r->pasar_nama;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Periode</label>
                            <div class="col-2">
                                <select class="form-control" name="lstBulan" id="lstBulan">
                                    <option value="">- SEMUA -</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <div class="col-2">
                                <select class="form-control" name="lstTahun" id="lstTahun">
                                    <?php
                                    $tahunawal      = (date('Y')-5);
                                    $tahunsekarang  = date('Y')+1;
                                    for($i=$tahunsekarang; $i>=$tahunawal; $i-=1) {
                                    ?>
                                        <option value="<?=$i;?>" <?=($i==date('Y')?'selected':'');?>><?=$i;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Tempat</label>
                            <div class="col-4">
                                <select class="form-control" name="lstTempat" id="lstTempat">
                                    <option value="">- SEMUA -</option>
                                    <?php foreach($listTempat as $r) { ?>
                                    <option value="<?=$r->tempat_id;?>"><?=$r->tempat_nama;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label text-right">Status</label>
                            <div class="col-4">
                                <select class="form-control" name="lstStatus" id="lstStatus">
                                    <option value="">- SEMUA -</option>
                                    <option value="1">BELUM BAYAR</option>
                                    <option value="2">BAYAR</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-9">
                                <a href="javascript:;" class="btn btn-primary" id="btn-print">
                                    <i class="fa fa-print"></i><span class="kt-hidden-mobile">Print Rekap</span>
                                </a>
                                <a href="javascript:;" class="btn btn-primary" id="btn-printdetail">
                                    <i class="fa fa-print"></i><span class="kt-hidden-mobile">Print Detail</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: { 
            lstPasar: { required: true }
        },
        messages: { 
            lstPasar: { required :'Pasar required' }
        }
    });

    $("#btn-print").click(function() {
        if($('#formInput').valid()) {
            printData();
        }
    });

    $("#btn-printdetail").click(function() {
        if($('#formInput').valid()) {
            printDataDetail();
        }
    });
});

function printData() {
    var pasar      = $('#lstPasar').val();
    var lstBulan   = $('#lstBulan').val();
    if (lstBulan == '') {
        var bulan = 'all';
    } else {
        var bulan = lstBulan;
    }
    var tahun      = $('#lstTahun').val();
    var lstStatus   = $('#lstStatus').val();
    if (lstStatus == '') {
        var status = 'all';
    } else {
        var status = lstStatus;
    }
    var lstTempat = $('#lstTempat').val();
    if (lstTempat == '') {
        var tempat = 'all';
    } else {
        var tempat = lstTempat;
    }
    var url = "<?=site_url('report/lap_retribusi/preview/');?>"+pasar+'/'+bulan+'/'+tahun+'/'+status+'/'+tempat;
    window.open(url, "_blank");
}

function printDataDetail() {
    var pasar      = $('#lstPasar').val();
    var lstBulan   = $('#lstBulan').val();
    if (lstBulan == '') {
        var bulan = 'all';
    } else {
        var bulan = lstBulan;
    }
    var tahun      = $('#lstTahun').val();
    var lstStatus   = $('#lstStatus').val();
    if (lstStatus == '') {
        var status = 'all';
    } else {
        var status = lstStatus;
    }
    var lstTempat = $('#lstTempat').val();
    if (lstTempat == '') {
        var tempat = 'all';
    } else {
        var tempat = lstTempat;
    }
    var url = "<?=site_url('report/lap_retribusi/previewdetail/');?>"+pasar+'/'+bulan+'/'+tahun+'/'+status+'/'+tempat;
    window.open(url, "_blank");
}
</script>