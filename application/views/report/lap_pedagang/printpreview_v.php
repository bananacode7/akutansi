<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Laporan Pedagang per Pasar</title>
<style type="text/css">
    table {
        border: 1px solid #ccccb3;
        width: 100%;
        border-collapse: collapse;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
        color: black;
        padding: 2px;
        border: 1px solid black;
        text-transform: uppercase;
    }

    td {
        padding: 3px;
        border: 1px solid black;
        vertical-align: top;
    }

    .page {
        width: 29.4cm;
        min-height: 21cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    body{
        font-family: "Franklin Gothic Medium"; 
        font-size:11px
    }

    h1{
        font-size:15px
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <div align="center" style="font-size: 15px;">LAPORAN PEDAGANG PER PASAR</div>
    <div align="center" style="font-size: 15px;"><?=$pasar;?></div>
    <br>
    <table align="center" width="100%">
        <tr>
            <th width="3%">No</th>
            <th width="10%">No. NPWRD</th>
            <th width="15%">Nama Pedagang</th>
            <th>Alamat</th>
            <th width="20%">Pasar</th>
            <th width="10%">Jenis Dagangan</th>
            <th width="10%">Status</th>
        </tr>
        <?php
        $pasar_id   = $this->uri->segment(4); 
        $jenis_id   = $this->uri->segment(5); 
        foreach ($listData as $r) {
            $tempat_id  = $r->tempat_id;
            if ($jenis_id != 'all') {
                $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_pendasaran', array('tempat_id' => $tempat_id, 'jenis_id' => $jenis_id, 'pasar_id' => $pasar_id))->result();
            } else {
                $listDetail = $this->db->order_by('penduduk_nik', 'asc')->get_where('v_pendasaran', array('tempat_id' => $tempat_id, 'pasar_id' => $pasar_id))->result();
            }
            if (count($listDetail) > 0) {
        ?>
        <tr>
            <th width="100%" colspan="7"><?=$r->tempat_nama;?></th>
        </tr>
        <?php 
        $no = 1;
        foreach($listDetail as $d) {
        ?>
        <tr>
            <td align="center" valign="top"><?=$no;?></td>
            <td valign="top" align="center"><?=$d->dasar_npwrd;?></td>
            <td valign="top"><?=$d->penduduk_nik.'<br>'.$d->penduduk_nama;?></td>
            <td valign="top"><?=$d->penduduk_alamat.' DESA '.$d->desa_nama.' KEC. '.$d->kecamatan_nama.'<br>'.$d->kabupaten_nama.' - '.$d->provinsi_nama; ?></td>
            <td valign="top"><?=$d->pasar_nama.'<br>BLOK '.$d->dasar_blok.' NOMOR '.$d->dasar_nomor.' LUAS '.$d->dasar_luas.' m2'; ?></td>
            <td valign="top"><?=$d->jenis_nama;?></td>
            <td valign="top" align="center"><?=strtoupper($d->dasar_status);?></td>
        </tr>
        <?php
                $no++;
            }
            }
        }
        ?>
    </table>
</div>
</body>
</html>