<?php
$username = $this->session->userdata('username');
$dataUser = $this->menu_m->select_user($username)->row();
?>
<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="#">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Profil</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Informasi Personal</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class=" container ">
            <div class="d-flex flex-row">
                <?=$_sidebaruser;?>

                <div class="flex-row-fluid ml-lg-8">
                    <div class="card card-custom card-stretch">
                        <div class="card-header py-3">
                            <div class="card-title align-items-start flex-column">
                                <h3 class="card-label font-weight-bolder text-dark">Informasi Personal</h3>
                                <span class="text-muted font-weight-bold font-size-sm mt-1">Update data Personal Anda</span>
                            </div>
                        </div>
                        <form class="form" name="formInput" id="formInput" method="post" enctype="multipart/form-data">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="image-input image-input-outline image-input-empty" id="kt_profile_avatar" style="background-image: url(<?=base_url('img/blank.png');?>)">
                                            <div class="image-input-wrapper" style="background-image: none;"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Ubah">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" name="foto" accept=".png, .jpg, .jpeg">
                                                <input type="hidden" name="foto_remove" value="1">
                                            </label>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Batal">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Hapus">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>
                                        <span class="form-text text-muted">File Type : png, jpg, jpeg.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9">
                                        <input class="form-control form-control-lg form-control-solid" name="nama" type="text" value="<?=$detail->user_name;?>" placeholder="Input Nama Lengkap" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Alamat</label>
                                    <div class="col-lg-9">
                                        <input class="form-control form-control-lg form-control-solid" name="alamat" type="text" value="<?=$detail->user_address;?>" placeholder="Input Alamat" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">No. Handphone / WA</label>
                                    <div class="col-lg-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="<?=$detail->user_phone;?>" placeholder="Input No. Handphone / WA" name="telp" autocomplete="off">
                                        </div>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Email</label>
                                    <div class="col-lg-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="<?=$detail->user_email;?>" placeholder="Input Email" name="email" autocomplete="off">
                                        </div>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-9">
                                       <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url('backend/assets/js/pages/custom/profile/profile.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: {
            nama: { required: true },
            alamat: { required: true },
            telp: { required: true, number: true },
            email: { required: true, email: true }
        },
        messages: {
            nama: { required :'Nama Lengkap required' },
            alamat: { required :'Alamat required' },
            telp: { required :'No. Handphone / WA required', number: 'Hanya ANGKA' },
            email: { required :'Email required', email:'Email tidak Valid' }
        },
        submitHandler: function (form) {
            var formData = new FormData($('#formInput')[0]);
            $.ajax({
                url: '<?=site_url('profil/updatedata');?>',
                type: "POST",
                dataType: 'json',
                data: formData,
                async: true,
                success: function(data) {
                    if (data.status === 'success') {
                        Swal.fire({
                            title:"Sukses",
                            text: "Update Data Profil Sukses",
                            showConfirmButton: false,
                            icon: "success",
                            timer: 2000
                        });
                        location.reload();
                    } else {
                        Swal.fire({
                            title:"Info",
                            text: "File tidak Sesuai",
                            timer: 2000,
                            showConfirmButton: false,
                            icon: "info"
                        });
                    }
                },
                error: function (response) {
                    Swal.fire({
                        title:"Error",
                        text: "Update Data Profil Gagal",
                        showConfirmButton: false,
                        icon: "error",
                        timer: 2000
                    });
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
});
</script>