<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="#">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Profil</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Ubah Password</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class=" container ">
            <div class="d-flex flex-row">
                <?=$_sidebaruser;?>

                <div class="flex-row-fluid ml-lg-8">
                    <div class="card card-custom card-stretch">
                        <div class="card-header py-3">
                            <div class="card-title align-items-start flex-column">
                                <h3 class="card-label font-weight-bolder text-dark">Ubah Password</h3>
                            </div>
                        </div>
                        <form class="form" name="formInput" id="formInput" method="post">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Password Baru</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control form-control-lg form-control-solid" name="newpassword" id="newpassword" placeholder="Input Password Baru" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Konfirmasi Password Baru</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control form-control-lg form-control-solid" name="confirmpassword" id="confirmpassword" placeholder="Input Konfirmasi Password Baru" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-9">
                                       <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Ubah Password</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url('backend/assets/js/pages/custom/profile/profile.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: {
            newpassword: { required: true, minlength: 5 },
            confirmpassword: { required: true, equalTo: "#newpassword" }
        },
        messages: {
            newpassword: {
                required:'Password Baru harus di isi', minlength:'Password Baru minimal 5 karakter'
            },
            confirmpassword: {
                required:'Konfirmasi Password harus di isi', minlength:'Konfirmasi Password minimal 5 karakter',
                equalTo: "Konfirmasi Password harus sama dengan Password Baru"
            }
        },
        submitHandler: function (form) {
            dataString = $("#formInput").serialize();
            $.ajax({
                url: "<?=site_url('profil/updatepassword');?>",
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        title:"Sukses",
                        text: "Update Password Berhasil",
                        showConfirmButton: false,
                        icon: "success",
                        timer: 2000
                    });
                    resetformPassword();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Update Data');
                }
            });
        }
    });
});

function resetformPassword() {
    $("#newpassword").val('')
    $("#confirmpassword").val('')
    
    var MValid = $("#formInput");
    MValid.validate().resetForm();
}
</script>