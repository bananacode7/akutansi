<?php
$meta     = $this->menu_m->select_meta()->row();
$username = $this->session->userdata('username');
$dataUser = $this->menu_m->select_user($username)->row();
?>
<!DOCTYPE html>
<html lang="en" >
<head>
	<base href="">
	<meta charset="utf-8"/>
	<title><?=$meta->meta_name;?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="Keuangan, Manujekulo, Sobatkom" />
	<meta name="author" content="Codedthemes" />
	<!-- Favicon icon -->
	<link rel="icon" href="<?=base_url('assets/images/favicon.ico')?>" type="image/x-icon">
	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
	<!-- waves.css -->
	<link rel="stylesheet" href="<?=base_url('assets/pages/waves/css/waves.min.css')?>" type="text/css" media="all">
	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/bootstrap/css/bootstrap.min.css')?>">
	<!-- waves.css -->
	<link rel="stylesheet" href="<?=base_url('assets/pages/waves/css/waves.min.css')?>" type="text/css" media="all">
	<!-- themify icon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/icon/themify-icons/themify-icons.css')?>">
	<!-- font-awesome-n -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/font-awesome-n.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/font-awesome.min.css')?>">
	<!-- scrollbar.css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/jquery.mCustomScrollbar.css')?>">
	<!-- datatables.css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/datatables/datatables.min.css')?>">
	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css')?>">
	<!-- Required Jquery -->
	<script type="text/javascript" src="<?=base_url('assets/js/jquery/jquery.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/jquery-ui/jquery-ui.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/popper.js/popper.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/jquery-validation/dist/jquery.validate.js');?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/jquery-validation/dist/jquery-validation.init.js');?>" type="text/javascript"></script>
</head>
<body>
	<div class="theme-loader">
		<div class="loader-track">
			<div class="preloader-wrapper">
				<div class="spinner-layer spinner-blue">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
				<div class="spinner-layer spinner-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

				<div class="spinner-layer spinner-yellow">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

				<div class="spinner-layer spinner-green">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?=$navbar?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?=$sidebar?>
					<div class="pcoded-content">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-block">
								<div class="row align-items-center">
									<div class="col-md-8">
										<div class="page-header-title">
											<h5 class="m-b-10">Dashboard</h5>
											<p class="m-b-0">Welcome to Material Able</p>
										</div>
									</div>
									<div class="col-md-4">
										<ul class="breadcrumb">
											<li class="breadcrumb-item">
												<a href="index.html"> <i class="fa fa-home"></i> </a>
											</li>
											<li class="breadcrumb-item"><a href="#!">Dashboard</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- Page-header end -->
						<div class="pcoded-inner-content">
							<?=$content?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- datatables -->
	<script src="<?=base_url('assets/datatables/datatables.min.js')?>"></script>
	<script src="<?=base_url('assets/weetAlert/sweetalert.min.js')?>"></script>
	
	<!-- waves js -->
	<script src="<?=base_url('assets/pages/waves/js/waves.min.js')?>"></script>
	<!-- jquery slimscroll js -->
	<script type="text/javascript" src="<?=base_url('assets/js/jquery-slimscroll/jquery.slimscroll.js')?>"></script>

	<!-- slimscroll js -->
	<script src="<?=base_url('assets/js/jquery.mCustomScrollbar.concat.min.js')?> "></script>

	<!-- menu js -->
	<script src="<?=base_url('assets/js/pcoded.min.js')?>"></script>
	<script src="<?=base_url('assets/js/vertical/vertical-layout.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/js/script.js')?>"></script>
</body>
</html>