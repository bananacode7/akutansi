<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Master</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Master Front End</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/social');?>" class="text-muted">Social Media</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon"><i class="flaticon2-list-1 text-primary"></i></span>
                        <h3 class="card-label">Daftar Social Media</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:;" class="btn btn-primary btn-icon-sm" data-toggle="modal" data-target="#formModalAdd">
                            <i class="fa fa-plus-circle"></i><span class="kt-hidden-mobile"> Tambah</span>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-hover" id="tableData">
                        <thead>
                            <tr>
                                <th width="6%"></th>
                                <th width="5%">No</th>
                                <th width="30%">Nama Sosial Media</th>
                                <th>URL</th>
                            </tr>
                        </thead>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/social/data_list');?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "dt-center",
            }
        ],
    });
});

$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: {
            nama: { required: true },
            lstClass: { required: true },
            url: { required: true, url: true }
        },
        messages: {
            nama: { required :'Nama Sosial Media required' },
            lstClass: { required :'Class required' },
            url: { required :'URL required', url:'URL tidak Valid'}
        },
        submitHandler: function (form) {
            dataString = $("#formInput").serialize();
            $.ajax({
                url: '<?=site_url('admin/social/savedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                        text: "Simpan Data Berhasil",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                    reload_table();
                },
                error: function() {
                    Swal.fire({
                        icon: "danger",
                        title: "Error",
                        text: "Simpan Data Gagal",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                }
            });
        }
    });
});

function resetformInput() {
    $("#nama").val('');
    $("#lstClass").val('');
    $("#url").val('');

    var MValid = $("#formInput");
    MValid.validate().resetForm();
    MValid.find(".error").removeClass("error");
    MValid.removeAttr('aria-describedby');
    MValid.removeAttr('aria-invalid');
}

function edit_data(id) {
    $('#formEdit')[0].reset();
    $.ajax({
        url : "<?=site_url('admin/social/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.social_id);
            $('#social_name').val(data.social_name);
            $('#social_class').val(data.social_class);
            $('#social_url').val(data.social_url);
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    $( "#formEdit" ).validate({
        rules: {
            nama: { required: true },
            lstClass: { required: true },
            url: { required: true, url: true }
        },
        messages: {
            nama: { required :'Nama Sosial Media required' },
            lstClass: { required :'Class required' },
            url: { required :'URL required', url:'URL tidak Valid'}
        },
        submitHandler: function (form) {
            dataString = $("#formEdit").serialize();
            $.ajax({
                url: '<?=site_url('admin/social/updatedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                        text: "Update Data Berhasil",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $('#formModalEdit').modal('hide');
                    reload_table();
                },
                error: function() {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "Update Data Gagal",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $('#formModalEdit').modal('hide');
                }
            });
        }
    });
});

function hapusData(social_id) {
    var id = social_id;
    Swal.fire({
        title: 'Anda Yakin ?',
        text: "Data ini akan di Hapus.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33'
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                url : "<?=site_url('admin/social/deletedata')?>/"+id,
                type: "POST",
                success: function(data) {
                    Swal.fire({
                        title:"Sukses",
                        text: "Hapus Data Sukses",
                        showConfirmButton: false,
                        icon: "success",
                        timer: 2000
                    });
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Hapus Data');
                }
            });
        }
    });
}

function printData() {
    var url = "<?=site_url('admin/social/preview');?>";
    window.open(url, "_blank");
}
</script>

<div class="modal fade" id="formModalAdd" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-add text-primary"></i> Form Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" method="post" name="formInput" id="formInput">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Sosial Media</label>
                        <input type="text" class="form-control" placeholder="Input Nama Sosial Media" name="nama" id="nama" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Class</label>
                        <select class="form-control" name="lstClass" id="lstClass" required>
                            <option value="">- Pilih -</option>
                            <option value="flaticon-facebook-logo-button">Facebook</option>
                            <option value="flaticon-twitter-logo-button">Twitter</option>
                            <option value="flaticon-instagram-logo">Instagram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input type="text" class="form-control" placeholder="Input URL" name="url" id="url" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="flaticon2-cancel-music"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="formModalEdit" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-edit text-primary"></i> Form Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" method="post" name="formEdit" id="formEdit">
            <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Sosial Media</label>
                        <input type="text" class="form-control" placeholder="Input Nama Sosial Media" name="nama" id="social_name" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Class</label>
                        <select class="form-control" name="lstClass" id="social_class" required>
                            <option value="">- Pilih -</option>
                            <option value="flaticon-facebook-logo-button">Facebook</option>
                            <option value="flaticon-twitter-logo-button">Twitter</option>
                            <option value="flaticon-instagram-logo">Instagram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input type="text" class="form-control" placeholder="Input URL" name="url" id="social_url" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="flaticon2-cancel-music"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>