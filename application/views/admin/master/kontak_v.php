<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Konfigurasi</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Kontak Kami</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon2-user text-primary"></i></span>
                        Kontak Kami
                    </h3>
                </div>

                <form class="form" id="formInput" name="formInput" method="post">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Nama Instansi</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="name" placeholder="Input Nama APP" value="<?=$detail->contact_name;?>" autocomplete="off" placeholder="Input Nama Instansi" autofocus />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Alamat</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="address" value="<?=$detail->contact_address; ?>" autocomplete="off" placeholder="Input Alamat" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">No. Telepon</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="phone" value="<?=$detail->contact_phone;?>" placeholder="Input No. Telepon" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Email</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="email" value="<?=$detail->contact_email; ?>" placeholder="Input Email" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Website</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="web" value="<?=$detail->contact_web;?>" placeholder="Input Website" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-9">
                               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: { 
            name: { required: true },
            address: { required: true },
            phone: { required: true },
            email: { required: true, email: true },
            web: { required: true, url: true }
        },
        messages: {
            name: { required:'Nama Instansi required' },
            address: { required:'Alamat required' },
            phone: { required:'No. Telepon harus diisi' },
            email: { required:'Email harus diisi', email:'Email tidak Valid' },
            web: { required:'Website harus diisi', url:'Website tidak Valid (Ex. http://www.domain.com)' }
        },
        submitHandler: function (form) {
            dataString = $('#formInput').serialize();
            $.ajax({
                url: "<?=site_url('admin/kontak/updatedata');?>",
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                        text: "Update Data Berhasil",
                        showConfirmButton: false,
                        timer: 2000
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Update Data');
                }
            });
        }
    });
});
</script>