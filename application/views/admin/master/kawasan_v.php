<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Master</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Master Umum</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/kawasan');?>" class="text-muted">Kawasan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon"><i class="flaticon2-list-1 text-primary"></i></span>
                        <h3 class="card-label">Daftar Kawasan</h3>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-hover" id="tableData">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Nama Kawasan</th>
                            </tr>
                        </thead>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/kawasan/data_list');?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "className": "dt-center",
            }
        ],
    });
});
</script>