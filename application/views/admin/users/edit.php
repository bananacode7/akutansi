<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Users</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/users');?>" class="text-muted">Users</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Edit Data</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon2-edit text-primary"></i></span>
                        Form Edit Data
                    </h3>
                    <div class="card-toolbar">
                        <a href="<?=site_url('admin/users');?>" class="btn btn-clean">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Batal</span>
                        </a>
                    </div>
                </div>

                <form class="form" id="formInput" name="formInput" method="post">
                <input type="hidden" name="id" value="<?=$detail->user_username;?>">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Username</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="username" value="<?=$detail->user_username;?>" autocomplete="off" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Password</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" name="password" id="password" autocomplete="off" placeholder="Input Password">
                                        <span class="form-text text-muted">*) Kosongi jika tidak diubah</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="name" autocomplete="off" placeholder="Input Nama Lengkap" value="<?=$detail->user_name;?>" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Alamat</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="alamat" autocomplete="off" value="<?=$detail->user_address;?>" placeholder="Input Alamat">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Email</label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" name="email" placeholder="Input Email" autocomplete="off" value="<?=$detail->user_email;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">No. Handphone</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" value="<?=$detail->user_phone;?>" name="phone" autocomplete="off" placeholder="Input No. Handphone">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Level</label>
                                    <div class="col-lg-9">
                                        <select class="form-control" name="lstLevel" id="lstLevel">
                                            <option value="">- Pilih Level -</option>
                                            <option value="Admin" <?=($detail->user_level == 'Admin'?'selected':'');?>>Admin</option>
                                            <option value="Viewer" <?=($detail->user_level == 'Viewer'?'selected':'');?>>Viewer</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9">
                                        <select class="form-control" name="lstStatus">
                                            <option value="">- Pilih Status -</option>
                                            <option value="Aktif" <?=($detail->user_status == 'Aktif'?'selected':'');?>>Aktif</option>
                                            <option value="Tidak Aktif" <?=($detail->user_status == 'Tidak Aktif'?'selected':'');?>>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="card-footer" align="center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function myLevel() {
    var level = document.getElementById("lstLevel"),value;
    $('#lstPasar').val('');
}

$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: {
            name: { required: true, minlength: 5 },
            email: { required: true, minlength: 15, email: true },
            lstLevel: { required: true },
            phone: { required: true, number: true },
            lstStatus: { required: true },
            lstPasar: {
                required: function(){
                    return $("#lstLevel").val()=='Operator';
                }
            }
        },
        messages: {
            name: { required:'Nama required', minlength:'Minimal 5 Karakter' },
            email: { required:'Email required', minlength:'Minimal 15 Karakter', email:'Email tidak Valid' },
            lstLevel: { required:'Level required' },
            phone: { required:'No. Handphone required', number:'Hanya Angka' },
            lstStatus: { required:'Status required' },
            lstPasar: { required:'Pasar required' }
        },
        submitHandler: function (form) {
            dataString = $('#formInput').serialize();
            $.ajax({
                url: "<?=site_url('admin/users/updatedata');?>",
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                        text: "Update Data Berhasil",
                        showConfirmButton: false,
                        timer: 2000
                    }).then(function() {
                        window.location="<?=site_url('admin/users');?>";
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Update Data');
                }
            });
        }
    });
});


</script>