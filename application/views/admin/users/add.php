<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Users</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/users');?>" class="text-muted">Users</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Tambah Data</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon2-add text-primary"></i></span>
                        Form Tambah Data
                    </h3>
                    <div class="card-toolbar">
                        <a href="<?=site_url('admin/users');?>" class="btn btn-clean">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Batal</span>
                        </a>
                    </div>
                </div>

                <form class="form" id="formInput" name="formInput" method="post">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Username</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Input Username" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Password</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" name="password" id="password" autocomplete="off" placeholder="Input Password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="name" autocomplete="off" placeholder="Input Nama Lengkap">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Alamat</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="alamat" autocomplete="off" placeholder="Input Alamat">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Email</label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Input Email" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">No. Handphone</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="phone" autocomplete="off" placeholder="Input No. Handphone">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Level</label>
                                    <div class="col-lg-9">
                                        <select class="form-control" name="lstLevel">
                                            <option value="">- Pilih Level -</option>
                                            <option value="Admin">Admin</option>
                                            <option value="Viewer">Viewer</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" align="center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Hany Huruf, Angka dan Garis Bawah");

$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: {
            username: {
                required: true, minlength: 5, alphanumeric: true,
                remote: {
                    url: "<?=site_url('admin/users/register_user_exists');?>",
                    type: "post",
                    data: {
                        username: function() {
                            return $("#username").val();
                        }
                    }
                }
            },
            password: { required: true, minlength: 5 },
            name: { required: true, minlength: 5 },
            alamat: { required: true },
            email: {
                required: true, minlength: 15, email: true,
                remote: {
                    url: "<?=site_url('admin/users/register_email_exists');?>",
                    type: "post",
                    data: {
                        email: function() {
                            return $("#email").val();
                        }
                    }
                }
            },
            phone: { required: true, number: true },
            lstLevel: { required: true }
        },
        messages: {
            username: {
                required:'Username required', minlength:'Minimal 5 Karakter',
                remote: 'Username sudah ada, mohon Ganti'
            },
            password: { required:'Password required', minlength:'Minimal 5 Karakter' },
            name: { required:'Nama required', minlength:'Minimal 5 Karakter' },
            alamat: { required:'Alamat required' },
            email: { required:'Email required', minlength:'Minimal 15 Karakter', email:'Email tidak Valid', 
                remote: 'Email sudah ada, mohon ganti' 
            },
            phone: { required:'No. Handphone required', number: 'Hanya Angka' },
            lstLevel: { required:'Level required' }
        },
        submitHandler: function (form) {
            dataString = $('#formInput').serialize();
            $.ajax({
                url: "<?=site_url('admin/users/savedata');?>",
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                        text: "Simpan Data Berhasil",
                        showConfirmButton: false,
                        timer: 2000
                    }).then(function() {
                        window.location="<?=site_url('admin/users');?>";
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Simpan Data');
                }
            });
        }
    });
});
</script>