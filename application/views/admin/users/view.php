<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Users</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/users');?>" class="text-muted">Users</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon"><i class="flaticon2-list-1 text-primary"></i></span>
                        <h3 class="card-label">Daftar Users</h3>
                    </div>
                    
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                            <i class="fas fa-filter"></i> Filter Data
                        </a>
                        &nbsp;
                        <a href="<?=site_url('admin/users/adddata');?>" class="btn btn-primary">
                            <i class="fa fa-plus-circle"></i> Tambah
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-hover" id="tableData">
                        <thead>
                            <tr>
                                <th width="5%"></th>
                                <th width="5%">No</th>
                                <th width="15%">Username</th>
                                <th width="15%">Nama Lengkap</th>
                                <th>Alamat</th>
                                <th width="15%">No. Handphone</th>
                                <th width="10%">Level</th>
                                <th width="10%">Status</th>
                            </tr>
                        </thead>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/users/data_list');?>",
            "type": "POST",
            "data": function(data) {
                data.lstLevel  = $('#lstLevel').val();
                data.lstStatus = $('#lstStatus').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 7 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 6, 7 ],
                "className": "dt-center",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fas fa-filter"></i> Filter Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Level</label>
                        <select class="form-control" name="lstLevel" id="lstLevel">
                            <option value="">- SEMUA DATA -</option>
                            <option value="Admin">Admin</option>
                            <option value="Viewer">Viewer</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="lstStatus" id="lstStatus">
                            <option value="">- SEMUA DATA -</option>
                            <option value="Aktif">Aktif</option>
                            <option value="Tidak Aktif">Tidak Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>