<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
	<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
	    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
	        <div class="d-flex align-items-center flex-wrap mr-1">
				<div class="d-flex align-items-baseline flex-wrap mr-5">
		            <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
	                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                	<li class="breadcrumb-item">
	                		<a href="#" class="text-muted">Informasi Statistik</a>
						</li>
	                </ul>
	            </div>
        	</div>
	    </div>
	</div>

	<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

	</div>
</div>