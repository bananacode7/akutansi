<form class="md-float-material form-material" id="signin_form" >
    <div class="text-center">
        <img src="<?=site_url('assets/images/logo.png')?>" alt="logo.png">
    </div>
    <div class="auth-box card">

      <div class="card-block">
        <div class="row m-b-20">
            <div class="col-md-12">
                <h3 class="text-center">Sign In</h3>
            </div>
        </div>
        <div class="alert alert-danger" role="alert" id="msgHide"></div>
        <div class="form-group form-info">
            <input autocomplete="off" type="text" id="username" name="username" class="form-control">
            <span class="form-bar"></span>
            <label class="float-label">Username</label>
        </div>
        <div class="form-group form-info">
            <input type="password" name="password" class="form-control">
            <span class="form-bar"></span>
            <label class="float-label">Password</label>
        </div>

        <div class="row m-t-30">
            <div class="col-md-12">
                <button class="btn btn-success btn-md btn-block waves-effect waves-light text-center m-b-20">Sign in</button>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-10">
                <p class="text-inverse text-left m-b-0">Terima Kasih</p>
                
            </div>
            <div class="col-md-2">
                <img src="<?=site_url('assets/images/auth/Logo-small-bottom.png')?>" alt="small-logo.png">
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">
    $.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[\w.]+$/i.test(value);
    }, "Hanya Huruf, Angka dan Garis Bawah");

    $(document).ready(function() {
        $("#msgHide").hide();
    });

    $(document).ready(function() {
        $("#signin_form").validate({
            rules: {
                username: { required: true, alphanumeric: true,
                    remote: {
                        url: "<?=site_url('login/check_login_exists');?>",
                        type: "post",
                        data: {
                            username: function() {
                                return $("#username").val();
                            }
                        }
                    } 
                },
                password: { required: true }
            },
            messages: {
                username: {
                    required :'Username harus diisi', remote: 'Username Anda tidak terdaftar'
                },
                password: {
                    required :'Password harus diisi'
                }
            },
            submitHandler: function (form) {
                dataString = $("#signin_form").serialize();
                $.ajax({
                    url: '<?=site_url('login/validasi');?>',
                    type: "POST",
                    data: dataString,
                    dataType: "JSON",
                    success: function(data) {
                        if (data.status === 'success') {
                            location.reload();
                        } else {
                            $('#msgHide').show();
                            $('#msgHide').html('<div class="alert-text" align="center">'+data.msg+'</div>');
                        }
                    },
                    error: function() {
                        $('#msgHide').show();
                        $('#msgHide').html('<div class="alert-text">Error Proses Login</div>');
                    }
                });
            }
        });
    });
</script>