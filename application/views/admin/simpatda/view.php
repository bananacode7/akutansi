<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Sync</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">SIMPATDA</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/wajibpajak');?>" class="text-muted">Wajib Pajak</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon"><i class="flaticon2-list-1 text-primary"></i></span>
                        <h3 class="card-label">Daftar Wajib Pajak</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                            <i class="fas fa-filter"></i> Filter Data
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-hover" id="tableData">
                        <thead>
                            <tr>
                                <th width="7%"></th>
                                <th width="5%">No</th>
                                <th width="10%">Tgl. Daftar</th>
                                <th width="10%">NPWPD/NPWRD</th>
                                <th width="25%">Nama Wajib Pajak</th>
                                <th>Alamat</th>
                                <th width="8%">Objek Pajak</th>
                            </tr>
                        </thead>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    $('.date-picker').datepicker({
        rtl: KTUtil.isRTL(),
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows
    });
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/wajibpajak/data_list');?>",
            "type": "POST",
            "data": function(data) {
                data.tgl_dari        = $('#tgl_dari').val();
                data.tgl_sampai      = $('#tgl_sampai').val();
                data.lstJenisDaftar  = $('#lstJenisDaftar').val();
                data.lstBidangUsaha  = $('#lstBidangUsaha').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 6 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 2, 6 ],
                "className": "dt-center",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fas fa-filter"></i> Filter Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Periode Daftar</label>
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" autocomplete="off" placeholder="DD-MM-YYYY"/>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                            </div>
                            <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" autocomplete="off" placeholder="DD-MM-YYYY" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Pendaftaran</label>
                        <select class="form-control" name="lstJenisDaftar" id="lstJenisDaftar">
                            <option value="">- SEMUA DATA -</option>
                            <option value="P">Wajib Pajak</option>
                            <option value="R">Wajib Retribusi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Bidang Usaha</label>
                        <select class="form-control" name="lstBidangUsaha" id="lstBidangUsaha">
                            <option value="">- SEMUA DATA -</option>
                            <option value="1">Pribadi</option>
                            <option value="2">Badan Usaha</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>