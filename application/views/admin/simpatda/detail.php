<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=site_url('admin/home');?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Menu Sync</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">SIMPATDA</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?=site_url('admin/wajibpajak');?>" class="text-muted">Wajib Pajak</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Detail Data</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon2-edit text-primary"></i></span>
                        Detail Data
                    </h3>
                    <div class="card-toolbar">
                        <a href="<?=site_url('admin/wajibpajak');?>" class="btn btn-warning">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Kembali</span>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <th scope="row" width="18%">Tanggal Pendaftaran</th>
                                <td width="2%">:</td>
                                <td width="80%"><?=tgl_indo($detail->t_tgldaftar);?></td>
                            </tr>
                            <tr>
                                <th scope="row">Jenis Pendaftaran</th>
                                <td>:</td>
                                <td><?=($detail->t_jenispendaftaran=='P'?'Wajib Pajak':'Wajib Retribusi');?></td>
                            </tr>
                            <tr>
                                <th scope="row">Bidang Usaha</th>
                                <td>:</td>
                                <td><?=($detail->t_bidangusaha=='1'?'Pribadi':'Badan Usaha');?></td>
                            </tr>
                            <tr>
                                <th scope="row">NPWPD</th>
                                <td>:</td>
                                <td><?=$detail->t_npwpd;?></td>
                            </tr>
                            <?php if ($detail->t_bidangusaha=='1') { ?>
                            <tr>
                                <th scope="row">N I K</th>
                                <td>:</td>
                                <td><?=$detail->t_nik;?></td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <th scope="row">NPWP</th>
                                <td>:</td>
                                <td><?=$detail->t_npwp;?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <th scope="row">Nama Wajib Pajak</th>
                                <td>:</td>
                                <td><?=$detail->t_nama;?></td>
                            </tr>
                            <tr>
                                <th scope="row">Alamat</th>
                                <td>:</td>
                                <td><?=$detail->t_alamat.' '.$detail->t_kodepos;?></td>
                            </tr>
                            <tr>
                                <th scope="row">No. Telepon</th>
                                <td>:</td>
                                <td><?=$detail->t_notelp;?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php if ($detail->t_bidangusaha=='2') { ?>
                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Milik Perusahaan</h3>
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <th scope="row" width="18%">N I K</th>
                                <td width="2%">:</td>
                                <td width="80%"><?=$detail->t_nik;?></td>
                            </tr>
                            <tr>
                                <th scope="row">Nama Pemilik</th>
                                <td>:</td>
                                <td><?=$detail->t_namapemilik;?></td>
                            </tr>
                            <tr>
                                <th scope="row">Alamat Pemilik</th>
                                <td>:</td>
                                <td><?=$detail->t_alamatpemilik;?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>