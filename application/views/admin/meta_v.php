<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <a href="<?=base_url();?>">
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Dashboard</h5>
                    </a>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Konfigurasi</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#" class="text-muted">Setting App</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container ">
            <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                <div class="card-header">
                    <h3 class="card-title">
                        <span class="card-icon"><i class="flaticon2-settings text-primary"></i></span>
                        Setting App
                    </h3>
                </div>

                <form class="form" id="formInput" name="formInput" method="post">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Nama App</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="name" placeholder="Input Nama APP" value="<?=$detail->meta_name;?>" autocomplete="off"  autofocus />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Deskripsi</label>
                            <div class="col-9">
                                <textarea class="form-control" rows="10" name="desc"><?=$detail->meta_desc;?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Keyword</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="keyword" placeholder="Input Keyword" value="<?=$detail->meta_keyword;?>" autocomplete="off"  />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Author</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="author" placeholder="Input Author" value="<?=$detail->meta_author;?>" autocomplete="off"  />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Developer</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="developer" placeholder="Input Developer" value="<?=$detail->meta_developer;?>" autocomplete="off"  />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Robots</label>
                            <div class="col-9">
                                <select class="form-control" name="lstRobot" >
                                    <option value="">- Pilih -</option>
                                    <option value="index, follow" <?php if ($detail->meta_robots=='index, follow') { echo 'selected'; } ?>>index, follow</option>
                                    <option value="index, nofollow" <?php if ($detail->meta_robots=='index, nofollow') { echo 'selected'; } ?>>index, nofollow</option>
                                    <option value="noindex, follow" <?php if ($detail->meta_robots=='noindex, follow') { echo 'selected'; } ?>>noindex, follow</option>
                                    <option value="noindex, nofollow" <?php if ($detail->meta_robots=='noindex, nofollow') { echo 'selected'; } ?>>noindex, nofollow</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Googlebots</label>
                            <div class="col-9">
                                <select class="form-control" name="lstGoogle" >
                                    <option value="">- Pilih -</option>
                                    <option value="index, follow" <?php if ($detail->meta_googlebots=='index, follow') { echo 'selected'; } ?>>index, follow</option>
                                    <option value="index, nofollow" <?php if ($detail->meta_googlebots=='index, nofollow') { echo 'selected'; } ?>>index, nofollow</option>
                                    <option value="noindex, follow" <?php if ($detail->meta_googlebots=='noindex, follow') { echo 'selected'; } ?>>noindex, follow</option>
                                    <option value="noindex, nofollow" <?php if ($detail->meta_googlebots=='noindex, nofollow') { echo 'selected'; } ?>>noindex, nofollow</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Jabatan</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="jabatan" placeholder="Input Jabatan" value="<?=$detail->meta_jabatan;?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">N I P</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="nip" placeholder="Input N I P" value="<?=$detail->meta_nip;?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Nama Pejabat</label>
                            <div class="col-9">
                                <input type="text" class="form-control" name="nama_pejabat" placeholder="Input Nama Pejabat" value="<?=$detail->meta_nama_pejabat;?>" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Jenis Target</label>
                            <div class="col-5">
                                <select class="form-control" name="lstTarget">
                                    <option value="">- Pilih Jenis Target -</option>
                                    <?php foreach($listTarget as $r) { ?>
                                    <option value="<?=$r->s_idtargetjenis;?>" <?=($r->s_idtargetjenis==$detail->meta_target?'selected':'');?>><?=$r->s_namatargetjenis;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-9">
                               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $( "#formInput" ).validate({
        rules: {
            name: { required: true },
            desc: { required: true },
            keyword: { required: true },
            author: { required: true },
            developer: { required: true },
            lstRobot: { required: true },
            lstGoogle: { required: true },
            lstTarget: { required: true }
        },
        messages: {
            name: { required :'Nama APP required' },
            desc: { required :'Description required' },
            keyword: { required :'Keyword required' },
            author: { required :'Author required' },
            developer: { required :'Developer required' },
            lstRobot: { required :'Robots required' },
            lstGoogle: { required :'Googlebots required' },
            lstTarget: { required :'Jenis Target required' }
        },
        submitHandler: function (form) {
            dataString = $('#formInput').serialize();
            $.ajax({
                url: "<?=site_url('admin/meta/updatedata');?>",
                type: "POST",
                data: dataString,
                success: function(data) {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                        text: "Update Data Berhasil",
                        showConfirmButton: false,
                        timer: 2000
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Update Data');
                }
            });
        }
    });
});
</script>