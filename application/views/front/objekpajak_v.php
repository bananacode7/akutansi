<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Objek Pajak</a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon-suitcase text-light"></i></span>
                            <h3 class="card-label text-light">Daftar Objek Pajak</h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                                <i class="fas fa-filter"></i> Filter Data
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="3%">No</th>
                                    <th width="10%">ID OP</th>
                                    <th width="15%">Nama Objek Pajak</th>
                                    <th width="13%">Jenis Pajak</th>
                                    <th>Alamat</th>
                                    <th width="10%">Telp</th>
                                </tr>
                            </thead>
                       </table>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('objek-pajak/data-list');?>",
            "type": "POST",
            "data": function(data) {
                data.lstJenisPajak  = $('#lstJenisPajak').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "className": "dt-center",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="fas fa-filter text-light"></i> Filter Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Jenis Pajak</label>
                        <select class="form-control" name="lstJenisPajak" id="lstJenisPajak">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach($listJenisPajak as $r) { ?>
                            <option value="<?=$r->s_idjenis;?>"><?=$r->s_namajenis;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>