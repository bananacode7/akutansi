<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Wajib Pajak</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Daftar Objek Pajak</a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon-users text-light"></i></span>
                            <h3 class="card-label text-light">Daftar Objek Pajak</h3>
                        </div>
                        <div class="card-toolbar">
                        <a href="<?=site_url('wajib-pajak');?>" class="btn btn-warning">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Kembali</span>
                        </a>
                    </div>
                    </div>
                    <div class="card-body">
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Wajib Pajak</h3>
                        <table class="table table-sm">
                            <tbody>
                                <tr>
                                    <th scope="row" width="18%">Tanggal Pendaftaran</th>
                                    <td width="2%">:</td>
                                    <td width="80%"><?=tgl_indo($detail->t_tgldaftar);?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Jenis Pendaftaran</th>
                                    <td>:</td>
                                    <td><?=($detail->t_jenispendaftaran=='P'?'Wajib Pajak':'Wajib Retribusi');?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Bidang Usaha</th>
                                    <td>:</td>
                                    <td><?=($detail->t_bidangusaha=='1'?'Pribadi':'Badan Usaha');?></td>
                                </tr>
                                <tr>
                                    <th scope="row">NPWPD</th>
                                    <td>:</td>
                                    <td><?=$detail->t_npwpd;?></td>
                                </tr>
                                <?php if ($detail->t_bidangusaha=='1') { ?>
                                <tr>
                                    <th scope="row">N I K</th>
                                    <td>:</td>
                                    <td><?=$detail->t_nik;?></td>
                                </tr>
                                <?php } else { ?>
                                <tr>
                                    <th scope="row">NPWP</th>
                                    <td>:</td>
                                    <td><?=$detail->t_npwp;?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <th scope="row">Nama Wajib Pajak</th>
                                    <td>:</td>
                                    <td><?=$detail->t_nama;?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Alamat</th>
                                    <td>:</td>
                                    <td><?=$detail->t_alamat.' '.$detail->t_kodepos;?></td>
                                </tr>
                            </tbody>
                        </table>
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Daftar Objek Pajak</h3>
                        <table class="table table-bordered table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="10%">ID OP</th>
                                    <th width="20%">Nama Objek</th>
                                    <th width="15%">Jenis Pajak</th>
                                    <th>Alamat</th>
                                    <th width="15%">No. Telp</th>
                                </tr>
                            </thead>
                       </table>
                    </div>
                </div>
            </div>
        </div>

	</div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

$(document).ready(function() {
    var t_idwp = '<?=$detail->t_idwp;?>';
    table = $('#tableData').DataTable({
        "info": false,
        "paging": false,
        "searching": false,
        "responsive": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('wajib-pajak/data-objek-list/');?>"+t_idwp,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "className": "dt-center",
            }
        ],
    });
});
</script>