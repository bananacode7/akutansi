<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Informasi Realisasi Pajak <?=date('Y');?></a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
            <div class="col-xl-3">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon-users text-light"></i></span>
                            <h3 class="card-label text-light">Realisasi Pajak <?=date('Y');?></h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                                <i class="flaticon2-search-1"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="80%">Jenis Pajak</th>
                                    <th width="20%">Total</th>
                                </tr>
                            </thead>
                       </table>
                    </div>
                </div>
            </div>
			<div class="col-xl-9">
				<div class="card card-custom gutter-b card-stretch">
    				<div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-line-chart text-light"></i></span>
                            <h3 class="card-label text-light">Statistik Realisasi Pajak <?=date('Y');?></h3>
                        </div>
    				</div>
    				<div class="card-body" style="position: relative;">
    					<div id="chart_realisasi"></div>
    				</div>
    			</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
var locale      = 'en';
var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
var formatter   = new Intl.NumberFormat(locale, options);

var options = {
    series: <?=$render_realisasi;?>,
    chart: {
        type: 'bar',
        height: 350,
        toolbar: {
            show: false
        }
    },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '40%',
            endingShape: 'rounded'
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    xaxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
    },
    yaxis: {
        title: {
            text: 'Jumlah Realisasi'
        }
    },
    fill: {
        opacity: 1
    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "Rp. " + formatter.format(val) + " Rupiah"
            }
        }
    }
};

var chart = new ApexCharts(document.querySelector("#chart_realisasi"), options);
chart.render();

$(document).ready(function() {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    $('.date-picker').datepicker({
        rtl: KTUtil.isRTL(),
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows
    });
});

function tabelPajak(tgl_dari, tgl_sampai) {
    table = $('#tableData').DataTable({
        "destroy": true,
        "info": false,
        "paging": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('realisasi-pajak/data-realisasi-list/');?>"+tgl_dari+'/'+tgl_sampai,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1],
                "orderable": false,
            },
            {
                "targets": [ 1 ],
                "className": "dt-right",
            }
        ],
    });
}

$(document).ready(function() {
    var tgl_dari   = $('#tgl_dari').val();
    var tgl_sampai = $('#tgl_sampai').val();
    tabelPajak(tgl_dari, tgl_sampai);

    $('#btn-filter').click(function() {
        var tgl_dari   = $('#tgl_dari').val();
        var tgl_sampai = $('#tgl_sampai').val();
        tabelPajak(tgl_dari, tgl_sampai);
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        var tgl_dari   = $('#tgl_dari').val();
        var tgl_sampai = $('#tgl_sampai').val();
        tabelPajak(tgl_dari, tgl_sampai);
        $('#filterData').modal('hide');
    });
});
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="flaticon2-search-1 text-light"></i> Cari Periode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Periode</label>
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" autocomplete="off" value="01-01-<?=date('Y');?>" />
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                            </div>
                            <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" autocomplete="off" value="<?=date('d-m-Y');?>" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>