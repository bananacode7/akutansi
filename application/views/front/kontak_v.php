<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Kontak Kami</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
    				<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
    				<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Kontak Kami</a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 pt-4">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-map text-primary"></i></span>
                            <h3 class="card-label">Kontak Kami</h3>
                        </div>
                    </div>
                    <div class="card-body" style="position: relative;">
                        <p><?=$detail->contact_name;?></p>
                        <p><?=$detail->contact_address;?></p>
                        <p>No. Telp <?=$detail->contact_phone;?></p>
                        <p><i class="flaticon-mail-1"></i> <a href="mailto://<?=$detail->contact_email;?>"><?=$detail->contact_email;?></a></p>
                        <p><i class="flaticon2-website"></i> <a href="<?=$detail->contact_web;?>"><?=$detail->contact_web;?></a></p>
                        <br>
                        <h5 class="font-size-lg text-dark font-weight-bold mb-6">Ikuti Kami :</h5>
                        <?php foreach($listSocial as $r) { ?>
                        <a href="<?=$r->social_url;?>" target="_blank"><i class="icon-2x <?=$r->social_class;?>"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
		</div>
        <div class="row">
            <div class="col-xl-6">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-body">
                        <img src="<?=base_url('img/kontak.png');?>" width="100%">
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-body">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.700072772237!2d110.83960731459212!3d-6.8062920950806385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70c4c53e0f6f59%3A0x34dc0fb9612c60d0!2sBPPKAD%20Kudus!5e0!3m2!1sid!2sid!4v1601813068039!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>