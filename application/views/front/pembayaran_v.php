<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Cek Pembayaran Pajak</a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container-fluid">
		<div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 pt-4">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-checking text-primary"></i></span>
                            <h3 class="card-label">Cek Pembayaran Pajak</h3>
                        </div>
                    </div>
                    <div class="card card-custom gutter-b">
                        <div class="card-header card-header-tabs-line">
                            <div class="card-toolbar">
                                <ul class="nav nav-tabs nav-bold nav-tabs-line">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab_1">
                                            <span class="nav-icon"><i class="flaticon2-calendar-5 text-success"></i></span>
                                            <span class="nav-text">Reklame</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab_2">
                                            <span class="nav-icon"><i class="flaticon-placeholder-3 text-danger"></i></span>
                                            <span class="nav-text">BPHTPB</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab_3">
                                            <span class="nav-icon"><i class="flaticon2-map text-warning"></i></span>
                                            <span class="nav-text">PBB</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="tab_1" role="tabpanel" aria-labelledby="tab_1">
                                    <?php require 'reklame_v.php'; ?>
                                </div>
                                <div class="tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="tab_2">
                                    <?php require 'bphtb_v.php'; ?>
                                </div>
                                <div class="tab-pane fade" id="tab_3" role="tabpanel" aria-labelledby="tab_3">
                                    <?php require 'simpbb_v.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>