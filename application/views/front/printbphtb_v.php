<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
    <title>Print Dokumen</title>
</head>
<style type="text/css">
page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 2cm;
    padding: 0.5cm;
}

page[size="A4"] {  
    width: 21cm;
    height: 29.7cm; 
}

body{
    font-family: "Tahoma";
    font-size: 14px;
}

table {
    border-collapse: collapse;
}

th {
    text-align: center;
    height: 15px;
}

th, td {
    padding: 3px;
}

@media print{
    #comments_controls,
    #print-link{
        display:none;
    }
}
</style>
<body>
<a href="#Print">
    <img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<page size="A4">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="20%" rowspan="4"><img src="<?=base_url('img/logo-kudus.png'); ?>" width="100px" height="120px"></td>
            <td width="80%" align="center" style="font-size: 20px;"><b>PEMERINTAH KABUPATEN KUDUS</b></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 25px;"><b><?=$kontak->contact_name;?></b></td>
        </tr>
        <tr>
            <td align="center"><?=$kontak->contact_address;?></td>
        </tr>
        <tr>
            <td align="center">Telp. <?=$kontak->contact_phone;?></td>
        </tr>
    </table>
    <hr style="height:2px; border-top:3px solid black; border-bottom:1px solid black;">
    <br>
    <div align="center" style="font-size: 15px;"><b>SURAT KETERANGAN STATUS WAJIB PAJAK DAERAH</b></div>
    <br><br>
    <p align="justify">Dengan ini diberitahukan bahwa berdasarkan basis data pelayanan pajak daerah pada BPPKAD Kabupaten Kudus disampaikan bahwa :</p>
    <table cellpadding="2" cellspacing="2" width="100%">
        <tr>
            <td width="19%" align="right">Nama</td>
            <td width="1%">:</td>
            <td width="80%"><?=$detail['nama'];?></td>
        </tr>
        <tr>
            <td align="right">NOP</td>
            <td>:</td>
            <td><?=$detail['nop'];?></td>
        </tr>
        <tr>
            <td align="right" valign="top">Alamat OP</td>
            <td valign="top">:</td>
            <td valign="top"><?=$detail['alamat'];?></td>
        </tr>
        <tr>
            <td align="right">Keperluan</td>
            <td>:</td>
            <td>Pembuatan Sertifikat</td>
        </tr>
    </table>
    <br>
    <div align="center"><b>STATUS : VALID</b></div>
    <br>
    <table cellpadding="2" cellspacing="2" border="1" width="70%" align="center">
        <tr>
            <td width="5%" align="center">No</td>
            <td width="45%" align="center">ID Billing</td>
            <td width="30%" align="center">BPHTB</td>
            <td width="20%" align="center">Status</td>
        </tr>
        <tr>
            <td align="center">1</td>
            <td align="center"><?=$detail['ntpd'];?></td>
            <td align="center">Rp. <?=$detail['bayar'];?>,-</td>
            <td align="center">LUNAS</td>
        </tr>
    </table>
    <br>
    <div align="justify">Demikian keterangan ini dibuat dalam rangka pemberian layanan publik tertentu pada tahun <?=date('Y');?></div>
    <br>
    <div align="justify">Demikian disampaikan, untuk dipergunakan sebagaimana mestinya.</div>
    <br>
    <table cellpadding="2" cellspacing="2" width="100%">
        <tr>
            <td width="50%"></td>
            <td width="50%" align="center">Kudus, <?=tgl_indo(date('Y-m-d'));?></td>
        </tr>
        <tr>
            <td></td>
            <td align="center"><?=$meta->meta_jabatan;?><br><br><br><br><br><u><?=$meta->meta_nama_pejabat;?></u><br>NIP. <?=$meta->meta_nip;?></td>
        </tr>
    </table>
</page>
</body>
</html>