<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
    <div class="container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex flex-column">
                <h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
                <div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Konfirmasi Pembayaran Pajak Reklame</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-checkmark text-light"></i></span>
                            <h3 class="card-label text-light">Konfirmasi Pembayaran Pajak Reklame</h3>
                        </div>
                    </div>
                    <form class="form" id="formInputReklame" name="formInputReklame" method="post">
                    <input type="hidden" name="npwpd_print" id="npwpd_print">
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-right">Masukan NPWPD Anda :</label>
                                <div class="col-lg-5">
                                    <div class="input-group">
                                        <input type="text" class="form-control" autocomplete="off" name="npwpd" id="npwpd" autofocus>
                                        <div class="input-group-append">
                                            <a href="javascript:;" class="btn btn-primary" id="btn-reklame">
                                                Cek Data
                                            </a>
                                        </div>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row hasilreklame">
                                <div class="col-md-12">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Wajib Pajak</h3>
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row" width="18%">Tanggal Daftar</th>
                                                <td width="2%">:</td>
                                                <td width="80%"><div id="tanggal"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Jenis Pendaftaran</th>
                                                <td>:</td>
                                                <td><div id="jenisdaftar"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Bidang Usaha</th>
                                                <td>:</td>
                                                <td><div id="bidangusaha"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">NPWPD</th>
                                                <td>:</td>
                                                <td><div id="nonpwpd"></div></td>
                                            </tr>
                                            <tr class="nik">
                                                <th scope="row">N I K</th>
                                                <td>:</td>
                                                <td><div id="nik"></div></td>
                                            </tr>
                                            <tr class="npwp">
                                                <th scope="row">NPWP</th>
                                                <td>:</td>
                                                <td><div id="npwp"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nama Wajib Pajak</th>
                                                <td>:</td>
                                                <td><div id="nama"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Alamat</th>
                                                <td>:</td>
                                                <td><div id="alamat"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">No. Telepon</th>
                                                <td>:</td>
                                                <td><div id="telp"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td></td>
                                                <td><a class="btn btn-primary" onclick="printData()" href="javascript:;"><i class="flaticon2-printer"></i> Cetak KSWP</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Daftar Pembayaran Pajak</h3>
                                    <table class="table table-bordered table-hover" id="tableData">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="13%">Tgl. Pendataan</th>
                                                <th width="25%">Jenis Reklame</th>
                                                <th>Lokasi</th>
                                                <th width="20%">Judul Reklame</th>
                                                <th width="15%">Masa Pajak</th>
                                                <th width="5%">Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#npwpd").inputmask({
    "mask": "P.9.9999999.99.99"
});

$(document).ready(function() {
    $('.hasilreklame').hide();
});

$(document).ready(function() {
    $( "#formInputReklame" ).validate({
        rules: {
            npwpd: {
                required: true,
                remote: {
                    url: "<?=site_url('reklame/cek-npwpd');?>",
                    type: "post",
                    data: {
                        npwpd: function() {
                            return $("#npwpd").val();
                        }
                    }
                }
            }
        },
        messages: {
            npwpd: {
                required:'NPWPD mohon diisi', remote: 'NPWPD Tidak ditemukan'
            }
        }
    });

    $("#btn-reklame").click(function() {
        if($('#formInputReklame').valid()) {
            tampilDataReklame();
        }
    });
});

function tampilDataReklame() {
    dataString = $('#formInputReklame').serialize();
    $.ajax({
        url : "<?=site_url('reklame/cek-data-npwpd');?>",
        type: "POST",
        dataType: "JSON",
        data: dataString,
        success: function(data) {
            if (data != null) {
                $('#tanggal').text(data.t_tgldaftar.split('-').reverse().join('-'));
                if (data.t_jenispendaftaran === 'P') {
                    jenisdaftar = 'Wajib Pajak';
                } else {
                    jenisdaftar = 'Wajib Retribusi';
                }
                $('#jenisdaftar').text(jenisdaftar);
                if (data.t_bidangusaha === '1') {
                    bidangusaha = 'Pribadi';
                    $('.nik').show();
                    $('.npwp').hide(); 
                } else {
                    bidangusaha = 'Badan Usaha';
                    $('.nik').hide();
                    $('.npwp').show();
                }
                $('#bidangusaha').text(bidangusaha);
                $('#npwpd_print').val(data.t_npwpd);
                $('#nonpwpd').text(data.t_npwpd);
                $('#nik').text(data.t_nik);
                $('#npwp').text(data.t_npwp);
                $('#nama').text(data.t_nama);
                $('#alamat').text(data.t_alamat+' '+data.t_kodepos);
                $('#telp').text(data.t_telp);
                $('.hasilreklame').show();
                t_idwp = data.t_idwp;
                tampilTagihan(t_idwp);
                $('#npwpd').val('');
            } else {
                $('.hasilreklame').hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function tampilTagihan(t_idwp) {
    table = $('#tableData').DataTable({
        "destroy": true,
        "info": false,
        "paging": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('reklame/data-tagihan-list/');?>"+t_idwp,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 6 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 5, 6 ],
                "className": "dt-center",
            }
        ],
    });
}

function printData() {
    var npwdp = $('#npwpd_print').val();
    var url   = "<?=site_url('reklame/preview/');?>"+npwdp;
    window.open(url, "_blank");
}
</script>