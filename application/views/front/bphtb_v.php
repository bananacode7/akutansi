<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
    <div class="container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex flex-column">
                <h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
                <div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a> -->
                    <!-- <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Konfirmasi Pembayaran Pajak BPHTB</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-checkmark text-light"></i></span>
                            <h3 class="card-label text-light">Konfirmasi Pembayaran Pajak BPHTB</h3>
                        </div>
                    </div>
                    <form class="form" id="formInputBPHTB" name="formInputBPHTB" method="post">
                    <input type="hidden" name="nop_print" id="nop_print">
                    <input type="hidden" name="billing_print" id="billing_print">
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-right">Masukan NOP Anda :</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" autocomplete="off" name="nop_bphtb" id="nop_bphtb" autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-right">Masukan ID Billing Anda :</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" autocomplete="off" name="ntpd" id="ntpd">
                                        <div class="input-group-append">
                                            <a href="javascript:;" class="btn btn-primary" id="btn-bphtb">
                                                Cek Data
                                            </a>
                                        </div>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row hasilbphtb">
                                <div class="col-md-12">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Wajib Pajak</h3>
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row" width="18%">NOP</th>
                                                <td width="2%">:</td>
                                                <td width="80%"><div id="nop_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">ID Billing</th>
                                                <td>:</td>
                                                <td><div id="ntpd_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nama Lengkap</th>
                                                <td>:</td>
                                                <td><div id="nama_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Alamat OP</th>
                                                <td>:</td>
                                                <td><div id="alamat_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kota</th>
                                                <td>:</td>
                                                <td><div id="kota_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Luas Tanah</th>
                                                <td>:</td>
                                                <td><div id="tanah_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Luas Bangunan</th>
                                                <td>:</td>
                                                <td><div id="bangun_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">BPHTB</th>
                                                <td>:</td>
                                                <td><div id="bayar_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Tanggal Pembayaran</th>
                                                <td>:</td>
                                                <td><div id="tanggal_bphtb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td></td>
                                                <td><a class="btn btn-primary" onclick="printData()" href="javascript:;"><i class="flaticon2-printer"></i> Cetak KSWP</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#nop_bphtb").inputmask({
    "mask": "99.99.999.999.999.9999.9"
});

$(document).ready(function() {
    $('.hasilbphtb').hide();
});

$(document).ready(function() {
    $( "#formInputBPHTB" ).validate({
        rules: {
            nop_bphtb: { required: true },
            ntpd: { required: true, number: true, minlength:11 }
        },
        messages: {
            nop_bphtb: { required:'NOP mohon diisi' },
            ntpd: { required:'NTPD mohon diisi', number:'NTPD Hanya Angka', minlength: 'Min. 11 Digit' }
        }
    });

    $("#btn-bphtb").click(function() {
        if($('#formInputBPHTB').valid()) {
            tampilDataBPHTB();
        }
    });
});

function tampilDataBPHTB() {
    dataString = $('#formInputBPHTB').serialize();
    $.ajax({
        url : "<?=site_url('bphtb/cek-data-bphtb');?>",
        type: "POST",
        dataType: "JSON",
        data: dataString,
        success: function(data) {
            if (data != null) {
                if (data.status === 'success') {
                    $('#nop_print').val(data.nop);
                    $('#billing_print').val(data.ntpd);
                    $('#nop_bphtb_view').text(data.nop);
                    $('#ntpd_bphtb_view').text(data.ntpd);
                    $('#nama_bphtb_view').text(data.nama);
                    $('#alamat_bphtb_view').text(data.alamat);
                    $('#kota_bphtb_view').text(data.kota);
                    $('#tanah_bphtb_view').text(data.luastanah);
                    $('#bangun_bphtb_view').text(data.luasbangun);
                    $('#bayar_bphtb_view').text('Rp. '+data.bayar+',-');
                    $('#tanggal_bphtb_view').text(data.tanggalbayar);
                    $('.hasilbphtb').show();
                    $('#nop_bphtb').val('');
                    $('#ntpd').val('');
                } else if(data.status === 'notfound') {
                    $('.hasilbphtb').hide();
                    $('#nop_bphtb').val('');
                    $('#ntpd').val('');
                    Swal.fire({
                        icon: "info",
                        title: "Info",
                        text: "Data Tidak di Temukan",
                        showConfirmButton: false,
                        timer: 2000
                    });
                } else if(data.status === 'errorAPI') {
                    $('.hasilbphtb').hide();
                    $('#nop_bphtb').val('');
                    $('#ntpd').val('');
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "API BPHTB tidak Terhubung",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            } else {
                $('.hasilbphtb').hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data BPTHB from API');
        }
    });
}

function printData() {
    var nop     = $('#nop_print').val();
    var billing = $('#billing_print').val();
    var url     = "<?=site_url('bphtb/preview/');?>"+nop+'/'+billing;
    window.open(url, "_blank");
}
</script>