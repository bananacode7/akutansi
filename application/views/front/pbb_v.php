<style type="text/css">
th .tagihan{
    text-align: center;
}
</style>
<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
    <div class="container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex flex-column">
                <h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
                <div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Konfirmasi Pembayaran Pajak PBB</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-checkmark text-light"></i></span>
                            <h3 class="card-label text-light">Konfirmasi Pembayaran Pajak PBB</h3>
                        </div>
                    </div>
                    <form class="form" id="formInputPBB" name="formInputPBB" method="post">
                    <input type="hidden" name="nop_print" id="nop_print">
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-right">Masukan NOP Anda :</label>
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <input type="text" class="form-control" autocomplete="off" name="nop_pbb" id="nop_pbb" autofocus>
                                        <div class="input-group-append">
                                            <a href="javascript:;" class="btn btn-primary" id="btn-pbb">
                                                Cek Data
                                            </a>
                                        </div>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row hasilpbb">
                                <div class="col-md-12">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Wajib Pajak</h3>
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row" width="18%">NOP</th>
                                                <td width="2%">:</td>
                                                <td width="80%"><div id="nop_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nama Lengkap</th>
                                                <td>:</td>
                                                <td><div id="nama_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Alamat OP</th>
                                                <td>:</td>
                                                <td><div id="alamat_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kota</th>
                                                <td>:</td>
                                                <td><div id="kota_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Luas Tanah</th>
                                                <td>:</td>
                                                <td><div id="tanah_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Luas Bangunan</th>
                                                <td>:</td>
                                                <td><div id="bangun_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">NJOP Tanah</th>
                                                <td>:</td>
                                                <td><div id="njop_tanah_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">NJOP Bangunan</th>
                                                <td>:</td>
                                                <td><div id="njop_bangun_pbb_view"></div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td></td>
                                                <td><a class="btn btn-primary" onclick="printData()" href="javascript:;"><i class="flaticon2-printer"></i> Cetak KSWP</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6 hasilpbb">Daftar Pembayaran Pajak</h3>
                                    <table class="table table-bordered hasilpbb">
                                        <thead>
                                            <tr>
                                                <td scope="col" align="center" width="10%" style="border-bottom: 0.5px solid #ebebeb;">TAHUN</td>
                                                <td scope="col" align="right" width="70%" style="border-bottom: 0.5px solid #ebebeb;">POKOK TAGIHAN</td>
                                                <td scope="col" align="center" width="20%" style="border-bottom: 0.5px solid #ebebeb;">STATUS</td>
                                            </tr>
                                        </thead>
                                        <tbody id="dataTagihan"></tbody>
                                    </table>
                                    <div>*) Pokok Tagihan belum termasuk Denda Keterlambatan.</div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#nop_pbb").inputmask({
    "mask": "99.99.999.999.999.9999.9"
});

$(document).ready(function() {
    $('.hasilpbb').hide();
});

$(document).ready(function() {
    $( "#formInputPBB" ).validate({
        rules: {
            nop_pbb: { required: true }
        },
        messages: {
            nop_pbb: { required:'NOP mohon diisi' }
        }
    });

    $("#btn-pbb").click(function() {
        if($('#formInputPBB').valid()) {
            tampilDataPBB();
        }
    });
});

function tampilDataPBB() {
    dataString = $('#formInputPBB').serialize();
    $.ajax({
        url : "<?=site_url('pbb/cek-data-pbb');?>",
        type: "POST",
        dataType: "JSON",
        data: dataString,
        success: function(data) {
            var locale    = 'en';
            var options   = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter = new Intl.NumberFormat(locale, options);
            var result=data;
            if (result != null) {
                if (result.status === 'success') {
                    $('#nop_print').val(result.nop);
                    $('#nop_pbb_view').text(result.nop);
                    $('#ntpd_pbb_view').text(result.ntpd);
                    $('#nama_pbb_view').text(result.nama);
                    $('#alamat_pbb_view').text(result.alamat);
                    $('#kota_pbb_view').text(result.kota);
                    $('#tanah_pbb_view').text(result.luastanah);
                    $('#bangun_pbb_view').text(result.luasbangun);
                    $('#njop_tanah_pbb_view').text(result.njop_tanah);
                    $('#njop_bangun_pbb_view').text(result.njop_bangunan);
                    $('#njop_bangun_pbb_view').text(result.njop_bangunan);
                    $('#nop_pbb').val('');
                    $('.itemPBB').remove();
                    $('.hasilpbb').show();
                    var html="";
                    for (var i = 0; i < result.item.length; i++) {
                        if (result.item[i].STATUS_PEMBAYARAN_SPPT == 0) {
                            var stsbayar = '<div style="color:#f54242"><b>BELUM BAYAR</b></div>';
                        } else {
                            var stsbayar = '<b>TERBAYAR</b>';
                        }
                        html +='<tr class="itemPBB">';
                        html +='<td align="center"> ' + result.item[i].THN_PAJAK_SPPT + '</td>';
                        html +='<td align="right">' + formatter.format(result.item[i].PBB_YG_HARUS_DIBAYAR_SPPT) + '</td>';
                        html +='<td align="center">' + stsbayar + '</td>';
                        html +='</tr>';
                    }
                    $('#dataTagihan').append(html);
                    $('.hasilpbb').show();
                } else if(data.status === 'notfound') {
                    $('#nop_pbb').val('');
                    $('.hasilpbb').hide();
                    Swal.fire({
                        icon: "info",
                        title: "Info",
                        text: "Data Tidak di Temukan",
                        showConfirmButton: false,
                        timer: 2000
                    });
                } else if(data.status === 'errorAPI') {
                    $('#nop_pbb').val('');
                    $('.hasilpbb').hide();
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "API BPHTB tidak Terhubung",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            } else {
                $('.hasilpbb').hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data PBB from API');
        }
    });
}

function printData() {
    var nop = $('#nop_print').val();
    var url = "<?=site_url('pbb/preview/');?>"+nop;
    window.open(url, "_blank");
}
</script>