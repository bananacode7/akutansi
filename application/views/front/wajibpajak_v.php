<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Wajib Pajak</a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon-users text-light"></i></span>
                            <h3 class="card-label text-light">Daftar Wajib Pajak</h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                                <i class="fas fa-filter"></i> Filter Data
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="7%"></th>
                                    <th width="5%">No</th>
                                    <th width="8%">Tgl. Daftar</th>
                                    <th width="10%">NPWPD/NPWRD</th>
                                    <th width="20%">Nama Wajib Pajak</th>
                                    <th>Alamat</th>
                                    <th width="8%">Objek Pajak</th>
                                </tr>
                            </thead>
                       </table>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('wajib-pajak/data-list');?>",
            "type": "POST",
            "data": function(data) {
                data.lstJenisDaftar  = $('#lstJenisDaftar').val();
                data.lstBidangUsaha  = $('#lstBidangUsaha').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 6 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 2, 6 ],
                "className": "dt-center",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

function edit_data(id) {
    $.ajax({
        url : "<?=site_url('wajib-pajak/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#tanggal').text(data.t_tgldaftar.split('-').reverse().join('-'));
            if (data.t_jenispendaftaran === 'P') {
                jenisdaftar = 'Wajib Pajak';
            } else {
                jenisdaftar = 'Wajib Retribusi';
            }
            $('#jenisdaftar').text(jenisdaftar);
            if (data.t_bidangusaha === '1') {
                bidangusaha = 'Pribadi';
                $('.npwp').hide();
                $('.perusahaan').hide();
            } else {
                bidangusaha = 'Bidang Usaha';
                $('.npwp').show();
                $('.perusahaan').show();
            }
            $('#bidangusaha').text(bidangusaha);
            $('#npwpd').text(data.t_npwpd);
            $('#npwp').text(data.t_npwp);
            $('#nama').text(data.t_nama);
            $('#alamat').text(data.t_alamat+' '+data.t_kodepos);
            $('#telp').text(data.t_notelp);
            $('#nama_perusahaan').text(data.t_namapemilik);
            $('#alamat_perusahaan').text(data.t_alamatpemilik);
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="fas fa-filter text-light"></i> Filter Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Jenis Pendaftaran</label>
                        <select class="form-control" name="lstJenisDaftar" id="lstJenisDaftar">
                            <option value="">- SEMUA DATA -</option>
                            <option value="P">Wajib Pajak</option>
                            <option value="R">Wajib Retribusi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Bidang Usaha</label>
                        <select class="form-control" name="lstBidangUsaha" id="lstBidangUsaha">
                            <option value="">- SEMUA DATA -</option>
                            <option value="1">Pribadi</option>
                            <option value="2">Badan Usaha</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="formModalEdit" tabindex="-1" role="dialog" aria-labelledby="formModalAdd" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="flaticon2-edit text-light"></i> Detail Wajib Pajak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <th scope="row" width="18%">Tanggal Daftar</th>
                            <td width="2%">:</td>
                            <td width="80%"><div id="tanggal"></div></td>
                        </tr>
                        <tr>
                            <th scope="row">Jenis Pendaftaran</th>
                            <td>:</td>
                            <td><div id="jenisdaftar"></div></td>
                        </tr>
                        <tr>
                            <th scope="row">Bidang Usaha</th>
                            <td>:</td>
                            <td><div id="bidangusaha"></div></td>
                        </tr>
                        <tr>
                            <th scope="row">NPWPD</th>
                            <td>:</td>
                            <td><div id="npwpd"></div></td>
                        </tr>
                        <tr class="npwp">
                            <th scope="row">NPWP</th>
                            <td>:</td>
                            <td><div id="npwp"></div></td>
                        </tr>
                        <tr>
                            <th scope="row">Nama Wajib Pajak</th>
                            <td>:</td>
                            <td><div id="nama"></div></td>
                        </tr>
                        <tr>
                            <th scope="row">Alamat</th>
                            <td>:</td>
                            <td><div id="alamat"></div></td>
                        </tr>
                        <tr>
                            <th scope="row">No. Telepon</th>
                            <td>:</td>
                            <td><div id="telp"></div></td>
                        </tr>
                        <tr class="perusahaan">
                            <th scope="row" colspan="3">Data Milik Perusahaan</th>
                        </tr>
                        <tr class="perusahaan">
                            <th scope="row">Nama Pemilik</th>
                            <td>:</td>
                            <td><div id="nama_perusahaan"></div></td>
                        </tr>
                        <tr class="perusahaan">
                            <th scope="row">Alamat Pemilik</th>
                            <td>:</td>
                            <td><div id="alamat_perusahaan"></div></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>