<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex flex-column">
				<h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
				<div class="d-flex align-items-center font-weight-bold my-2">
                    <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
        			<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
    				<a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Informasi Realisasi Pajak <?=date('Y');?></a>
                </div>
        	</div>
		</div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon-users text-light"></i></span>
                            <h3 class="card-label text-light">Realisasi Pajak <?=date('Y');?></h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                                <i class="flaticon2-search-1"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="30%" rowspan="2">Jenis Pajak</th>
                                    <th width="10%" rowspan="2">Target Anggaran</th>

                                    <th width="20%" colspan="2">Realisasi</th>
                                    <th width="10%" rowspan="2">%</th>
                                    <th width="20%" colspan="2">Selisih</th>
                                </tr>
                                <tr>
                                    <th width="10%">s/d Bulan Lalu</th>
                                    <th width="10%">s/d Bulan ini</th>
                                    <th width="15%">Target</th>
                                    <th width="5%">%</th>
                                </tr>
                            </thead>
                            <tbody id="dataRealisasi">
                            </tbody>
                       </table>
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>

<script type="text/javascript">
function getData(){
    $.ajax({
        url : "<?=site_url('front/realisasi/cek_realisasi');?>",
        type: "POST",
        dataType: "html",
        success: function(data) {
            $('#dataRealisasi').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    getData();
});
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="flaticon2-search-1 text-light"></i> Cari Periode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Periode</label>
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" autocomplete="off" value="01-01-<?=date('Y');?>" />
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                            </div>
                            <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" autocomplete="off" value="<?=date('d-m-Y');?>" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>