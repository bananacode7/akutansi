<div class="subheader py-2 py-lg-12  subheader-transparent " id="kt_subheader">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex flex-column">
                <h2 class="text-white font-weight-bold my-2 mr-5">Beranda</h2>
                <div class="d-flex align-items-center font-weight-bold my-2">
                    <!-- <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a> -->
                    <!-- <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span> -->
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Beranda</a>
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="<?=base_url();?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Informasi Realisasi Pajak <?=date('Y');?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header border-0 bg-primary">
                        <div class="card-title">
                            <span class="card-icon"><i class="flaticon2-line-chart text-light"></i></span>
                            <h3 class="card-label text-light">Realisasi Pajak <?=date('Y');?></h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-warning btn-icon-sm" data-toggle="modal" data-target="#filterData">
                                <i class="flaticon2-search-1"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="text-align:center; background-color: #dedede;">Jenis Pajak</th>
                                    <th width="15%" rowspan="2" style="text-align:center; background-color: #dedede;">Target Anggaran</th>
                                    <th width="25%" colspan="3" style="text-align:center; background-color: #dedede;">Realisasi</th>
                                    <th width="20%" colspan="2" style="text-align:center; background-color: #dedede;">Selisih</th>
                                </tr>
                                <tr>
                                    <th width="10%" style="text-align:center; background-color: #dedede;">s/d Bulan Lalu</th>
                                    <th width="10%" style="text-align:center; background-color: #dedede;">s/d Bulan ini</th>
                                    <th width="5%" style="text-align:center; background-color: #dedede;">%</th>
                                    <th width="15%" style="text-align:center; background-color: #dedede;">Target</th>
                                    <th width="5%" style="text-align:center; background-color: #dedede;">%</th>
                                </tr>
                            </thead>
                       </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    var bulan  = $('#lstBulan').val();
    var tahun  = $('#tahun').val();
    var target = $('#lstJenisTarget').val();
    tabelPajak(bulan, tahun, target);

    $('#btn-filter').click(function() {
        var bulan  = $('#lstBulan').val();
        var tahun  = $('#tahun').val();
        var target = $('#lstJenisTarget').val();
        tabelPajak(bulan, tahun, target);
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        var bulan = $('#lstBulan').val();
        var tahun = $('#tahun').val();
        var target = $('#lstJenisTarget').val();
        tabelPajak(bulan, tahun, target);
        $('#filterData').modal('hide');
    });
});

function tabelPajak(bulan, tahun, target) {
    table = $('#tableData').DataTable({
        "destroy": true,
        "info": false,
        "paging": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('realisasi-pajak/data-realisasi-list/');?>"+bulan+'/'+tahun+'/'+target,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6 ],
                "orderable": false,
            },
            {
                "targets": [ 1, 2, 3, 5 ],
                "className": "dt-right",
            },
            {
                "targets": [ 4, 6 ],
                "className": "dt-center",
            }
        ],
    });
}
</script>

<div class="modal fade" id="filterData" tabindex="-1" role="dialog" aria-labelledby="filterData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="flaticon2-search-1 text-light"></i> Cari Periode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form-filter">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-7 form-group-sub">
                            <label class="form-control-label">Bulan</label>
                            <select class="form-control" name="lstBulan" id="lstBulan">
                                <option value="1" <?=(date('m')==1?'selected':'');?>>Januari</option>
                                <option value="2" <?=(date('m')==2?'selected':'');?>>Februari</option>
                                <option value="3" <?=(date('m')==3?'selected':'');?>>Maret</option>
                                <option value="4" <?=(date('m')==4?'selected':'');?>>April</option>
                                <option value="5" <?=(date('m')==5?'selected':'');?>>Mei</option>
                                <option value="6" <?=(date('m')==6?'selected':'');?>>Juni</option>
                                <option value="7" <?=(date('m')==7?'selected':'');?>>Juli</option>
                                <option value="8" <?=(date('m')==8?'selected':'');?>>Agustus</option>
                                <option value="9" <?=(date('m')==9?'selected':'');?>>September</option>
                                <option value="10" <?=(date('m')==10?'selected':'');?>>Oktober</option>
                                <option value="11" <?=(date('m')==11?'selected':'');?>>November</option>
                                <option value="12" <?=(date('m')==12?'selected':'');?>>Desember</option>
                            </select>
                        </div>
                        <div class="col-lg-5 form-group-sub">
                            <label class="form-control-label">Tahun</label>
                            <input type="text" class="form-control" name="tahun" id="tahun" value="<?=date('Y');?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Target</label>
                        <select class="form-control" name="lstJenisTarget" id="lstJenisTarget">
                            <?php foreach($listTarget as $r) { ?>
                            <option value="<?=$r->s_idtargetjenis;?>" <?=($r->s_idtargetjenis==$meta->meta_target?'selected':'');?>><?=$r->s_namatargetjenis;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="flaticon2-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>