<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
    <title>Print Dokumen</title>
</head>
<style type="text/css">
page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 2cm;
    padding: 0.5cm;
}

page[size="A4"] {  
    width: 21cm;
    height: 29.7cm; 
}

body{
    font-family: "Tahoma";
    font-size: 14px;
}

table {
    width: 100%;
    border-collapse: collapse;
}

th {
    text-align: center;
    height: 15px;
}

th, td {
    padding: 3px;
}

@media print{
    #comments_controls,
    #print-link{
        display:none;
    }
}
</style>
<body>
<a href="#Print">
    <img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<page size="A4">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="20%" rowspan="4"><img src="<?=base_url('img/logo-kudus.png'); ?>" width="100px" height="120px"></td>
            <td width="80%" align="center" style="font-size: 20px;"><b>PEMERINTAH KABUPATEN KUDUS</b></td>
        </tr>
        <tr>
            <td align="center" style="font-size: 25px;"><b><?=$kontak->contact_name;?></b></td>
        </tr>
        <tr>
            <td align="center"><?=$kontak->contact_address;?></td>
        </tr>
        <tr>
            <td align="center">Telp. <?=$kontak->contact_phone;?></td>
        </tr>
    </table>
    <hr style="height:2px; border-top:3px solid black; border-bottom:1px solid black;">
    <br>
    <div align="center" style="font-size: 15px;"><b>SURAT KETERANGAN STATUS WAJIB PAJAK DAERAH</b></div>
    <br><br>
    <p align="justify">Dengan ini diberitahukan bahwa berdasarkan basis data pelayanan pajak daerah pada BPPKAD Kabupaten Kudus disampaikan bahwa :</p>
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td width="19%" align="right">Nama</td>
            <td width="1%">:</td>
            <td width="80%"><?=$detail->t_nama;?></td>
        </tr>
        <tr>
            <td align="right">NPWPD</td>
            <td>:</td>
            <td><?=$detail->t_npwpd;?></td>
        </tr>
        <tr>
            <td align="right" valign="top">Alamat</td>
            <td valign="top">:</td>
            <td valign="top"><?=$detail->t_alamat.' '.$detail->t_kodepos;?></td>
        </tr>
        <tr>
            <td align="right">Keperluan</td>
            <td>:</td>
            <td>Perizinan Reklame</td>
        </tr>
    </table>
    <br>
    <div align="center"><b>STATUS : <?=(count($dataStatus)>0?'TIDAK VALID':'VALID');?></b></div>
    <?php if (count($dataStatus)>0) { ?>
    <br>
    <div align="justify">Karena memiliki tagihan pajak daerah sebagai berikut :</div>
    <?php } ?>
    <br>
    <table cellpadding="2" cellspacing="2" border="1">
        <tr>
            <td width="5%" align="center">No</td>
            <td width="45%" align="center">Jenis Reklame</td>
            <td width="25%" align="center">Masa Pajak</td>
            <td width="15%" align="center">Total</td>
            <td width="10%" align="center">Status</td>
        </tr>
        <?php 
        $no    = 1;
        $total = 0;
        foreach($listReklame as $r) {
        ?>
        <tr>
            <td align="center" valign="top" style="font-size: 12px;"><?=$no;?></td>
            <td valign="top" style="font-size: 12px;"><?=strtoupper($r->s_namareklamejenis);?><br>LOKASI : <?=strtoupper($r->t_lokasi);?></td>
            <td align="center" valign="top" style="font-size: 12px;"><?=date('d-m-Y', strtotime($r->t_masaawal)) . ' s/d ' . date('d-m-Y', strtotime($r->t_masaakhir));?></td>
            <td align="right" valign="top" style="font-size: 12px;"><?=number_format($r->t_jmlhpajak,0,'','.');?></td>
            <td align="center" valign="top" style="font-size: 12px;"><?=($r->t_tglpembayaran != ''?'LUNAS':'BELUM LUNAS');?></td>
        </tr>
        <?php 
            $total = ($total+$r->t_jmlhpajak);
            $no++;
        }
        ?>
        <tr>
            <td colspan="3" align="center"><b>TOTAL</b></td>
            <td align="right"><b><?=number_format($total,0,'','.');?></b></td>
            <td></td>
        </tr>
    </table>
    <br>
    <div align="justify">Demikian keterangan ini dibuat dalam rangka pemberian layanan publik tertentu pada tahun <?=date('Y');?></div>
    <br>
    <div align="justify">Demikian disampaikan, untuk dipergunakan sebagaimana mestinya.</div>
    <br>
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td width="50%"></td>
            <td width="50%" align="center">Kudus, <?=tgl_indo(date('Y-m-d'));?></td>
        </tr>
        <tr>
            <td></td>
            <td align="center"><?=$meta->meta_jabatan;?><br><br><br><br><br><u><?=$meta->meta_nama_pejabat;?></u><br>NIP. <?=$meta->meta_nip;?></td>
        </tr>
    </table>
</page>
</body>
</html>