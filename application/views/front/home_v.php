<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(<?=base_url('backend/assets/media/svg/shapes/abstract-2.svg');?>)">
                    <div class="card-body">
                        <p align="center">
                            <img src="<?=base_url('img/logo-kudus.png');?>" width="130px"><br>
                            <img alt="Logo" src="<?=base_url('img/logo-depan.png');?>" class="logo-default max-h-80px"/>
                            <p class="text-dark-75 font-weight-bolder font-size-h5 m-0" align="center">Sistem Informasi Status Wajib Pajak Daerah<br>Kabupaten Kudus</p>
                        </p>
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-xl-4">
                <div class="card card-custom card-stretch gutter-b bg-primary">
                    <div class="card-body d-flex align-items-center py-0 mt-8">
                        <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                            <a href="<?=site_url('wajib-pajak');?>" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-white">Wajib Pajak</a>
                            <span class="font-weight-bold text-light  font-size-lg">Informasi tentang Wajib Pajak</span>
                        </div>
                        <img src="<?=base_url('img/wajibpajak.png');?>" alt="" class="align-self-end h-100px">
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card card-custom card-stretch gutter-b bg-danger">
                    <div class="card-body d-flex align-items-center py-0 mt-8">
                        <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                            <a href="<?=site_url('objek-pajak');?>" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-white">Objek Pajak</a>
                            <span class="font-weight-bold text-light font-size-lg">Informasi tentang Objek Pajak</span>
                        </div>
                        <img src="<?=base_url('img/objekpajak.png');?>" alt="" class="align-self-end h-100px">
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card card-custom card-stretch gutter-b bg-warning">
                    <div class="card-body d-flex align-items-center py-0 mt-8">
                        <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                            <a href="<?=site_url('reklame');?>" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-white">Reklame</a>
                            <span class="font-weight-bold text-light font-size-lg">Konfirmasi Status Pajak Reklame</span>
                        </div>
                        <img src="<?=base_url('img/reklame.png');?>" alt="" class="align-self-end h-100px">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-4">
                <div class="card card-custom card-stretch gutter-b bg-success">
                    <div class="card-body d-flex align-items-center py-0 mt-8">
                        <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                            <a href="<?=site_url('bphtb');?>" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-white">BPHTB</a>
                            <span class="font-weight-bold text-light font-size-lg">Konfirmasi Status Pajak BPHTB</span>
                        </div>
                        <img src="<?=base_url('img/bphtb.png');?>" alt="" class="align-self-end h-100px">
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card card-custom card-stretch gutter-b bg-faded">
                    <div class="card-body d-flex align-items-center py-0 mt-8">
                        <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                            <a href="<?=site_url('pbb');?>" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-danger">PBB</a>
                            <span class="font-weight-bold text-muted font-size-lg">Konfirmasi Status Pajak PBB</span>
                        </div>
                        <img src="<?=base_url('img/pbb.png');?>" alt="" class="align-self-end h-100px">
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card card-custom card-stretch gutter-b bg-dark">
                    <div class="card-body d-flex align-items-center py-0 mt-8">
                        <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                            <a href="<?=site_url('realisasi-pajak');?>" class="card-title font-weight-bolder text-inverse-dark font-size-h5 mb-2 text-hover-danger">Realisasi Pajak</a>
                            <span class="font-weight-bold text-light font-size-lg">Statistik Realisasi Pajak</span>
                        </div>
                        <img src="<?=base_url('img/realisasi.png');?>" alt="" class="align-self-end h-100px">
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>