<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lap_detailpedagang_m extends CI_Model
{
    public $table         = 'v_pendasaran_view';
    public $column_order  = array(null, null, 'dasar_no', 'dasar_sampai', 'dasar_npwrd', 'penduduk_nama', 'pasar_nama', null);
    public $column_search = array('dasar_no', 'dasar_sampai', 'dasar_npwrd', 'penduduk_nik', 'penduduk_nama', 'pasar_nama');
    public $order         = array('dasar_sampai' => 'desc');

    public $table1         = 'v_skrd';
    public $column_order1  = array(null, 'skrd_no', 'skrd_tgl', 'skrd_bulan', 'skrd_tgl_bayar', 'skrd_tipe_bayar', 'skrd_total');
    public $column_search1 = array('skrd_no');
    public $order1         = array('skrd_tgl' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('lstPasar', 'true')) {
            $this->db->where('pasar_id', $this->input->post('lstPasar', 'true'));
        }
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('dasar_sampai >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('dasar_sampai <=', $tgl_sampai);
        }
        if ($this->input->post('lstTempat', 'true')) {
            $this->db->where('tempat_id', $this->input->post('lstTempat', 'true'));
        }
        if ($this->input->post('lstStatusDasar', 'true')) {
            $this->db->where('dasar_status', $this->input->post('lstStatusDasar', 'true'));
        }

        $this->db->from($this->table);
        $this->db->where('dasar_acc', 2);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        $this->db->where('dasar_acc', 2);

        return $this->db->count_all_results();
    }

    // Detail
    private function _get_detail_datatables_query($dasar_id)
    {
        $this->db->from($this->table1);
        $this->db->where('dasar_id', $dasar_id);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($dasar_id)
    {
        $this->_get_detail_datatables_query($dasar_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($dasar_id)
    {
        $this->_get_detail_datatables_query($dasar_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($dasar_id)
    {
        $this->db->from($this->table1);
        $this->db->where('dasar_id', $dasar_id);

        return $this->db->count_all_results();
    }
}
/* Location: ./application/model/report/Lap_detailpedagang_m.php */
