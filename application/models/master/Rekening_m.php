<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening_m extends CI_Model {
	public $table         = 'v_master_reg';
    public $column_order  = array(null, 'masreg_id', 'masreg_kode', 'masreg_nama','masreg_index','masreg_sub','masreg_level','masreg_status');
    public $column_search = array('masreg_id', 'masreg_kode', 'masreg_nama','masreg_index','masreg_sub','masreg_level','masreg_status');
    public $order         = array('urut' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }
    private function _get_datatables_query()
    {
        if ($this->input->post('status', 'true')) {
            $this->db->where('status=', $this->input->post('status', 'true'));
        }
        if ($this->input->post('index', 'true')) {
            $this->db->where('masreg_index =', $this->input->post('index', 'true'));
        }
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function insert_data($post){
        $uraian = strtoupper(stripHTMLtags($post['uraian']));
        if ($post['index']=='head') {
            $sql="call insert_master_reg('".$uraian."','head','')";
        }else{
            $sql="call insert_master_reg('".$uraian."','','".$post['sub']."')";
        }
        $this->db->query($sql);
    }
    function update_data($post){

        $data         = array(
            'masreg_nama'   => strtoupper(stripHTMLtags($post['uraian'])),
            'masreg_status' => $post['status'],
            'masreg_update' => date('Y-m-d H:i:s')
        );
        $this->db->where('masreg_kode', $post['sub']);
        $this->db->update('keuangan_master_reg', $data);
    }
    function deletedata($id){
      
      $this->db->where('masreg_id', $id);
      $this->db->delete('keuangan_master_reg');
  }
}

/* End of file Keuangan_m.php */
/* Location: ./application/models/Keuangan_m.php */