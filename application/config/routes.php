<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller']   = 'login';
$route['rekening']             = 'master/rekening';
$route['unit']                 = 'master/unit';
$route['tahun']                = 'master/tahun';
$route['404_override']         = 'my_error';
$route['translate_uri_dashes'] = false;

