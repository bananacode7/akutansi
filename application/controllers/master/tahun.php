<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tahun extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('master/tahun_m');
    }

    public function index()
    {
        $this->template->layout('master/tahun_v');
    }

    public function data_list()
    {
        $List = $this->tahun_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row           = array();
            $thn_id       = $r->thn_id;
            
            $row[]         = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $thn_id . "'" . ')"><i class="ti-pencil text-primary"></i></a>
            <a href="javascript:;" onclick="hapusData(' . $thn_id . ')" title="Hapus Data"><i class="ti-trash text-danger"></i></a>';
            $row[]  = $no;
            $row[]  = $r->thn_nama;

            $data[] = $row;

        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->tahun_m->count_all(),
            "recordsFiltered" => $this->tahun_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function nama_exists($nama)
    {
        $this->db->where('thn_nama', $nama);
        $query = $this->db->get('thn_nama');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_nama_exists()
    {
        if (array_key_exists('nama', $_POST)) {
            if ($this->nama_exists(stripHTMLtags($this->input->post('nama', 'true'))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->tahun_m->insert_data();
        // $data = array(
        //     'jenis_unit_ket'   => strtoupper(stripHTMLtags($this->input->post('keterangan', 'true'))),
        //     'thn_nama'   => $this->input->post('jam', 'true'),
        //     'jenis_unit_update' => date('Y-m-d H:i:s'),
        // );
        // echo json_encode($data);
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('keuangan_tahun', array('thn_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->tahun_m->update_data();
    }

    public function deletedata($id)
    {
        $this->tahun_m->delete_data($id);
    }
}
/* Location: ./application/controller/master/Jenis_unit.php */
