<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('master/rekening_m');
		$this->load->library('template');
	}
	public function index()
	{

		$this->template->layout('master/v_rekening');

	}
	function get_data($id){

		$data = $this->db->get_where('v_master_reg', array('masreg_id' => $id))->row();
		echo json_encode($data);

	}
	public function data_list()
	{
		$List = $this->rekening_m->get_datatables();
		$data = array();
		$no   = $_POST['start'];
		foreach ($List as $r) {
			$no++;
			$row       = array();
			$masreg_id = $r->masreg_id;
			$row[]     = '<a href="#" onclick=pilih_aksi('.$masreg_id.')><i class="ti-pencil text-primary"></i></a>
			<a href="#" onclick=hapusData('.$masreg_id.')><i class="ti-trash text-danger"></i></a>';
			$row[]     = $no;
			$row[]     = $r->masreg_kode;
			$row[]     = $r->masreg_nama;

			$row[]     = $r->masreg_status;

			$data[]    = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->rekening_m->count_all(),
			"recordsFiltered" => $this->rekening_m->count_filtered(),
			"data"            => $data,
		);

		echo json_encode($output);
	}
	public function data_list_modal()
	{
		$List = $this->rekening_m->get_datatables();
		$data = array();
		$no   = $_POST['start'];
		foreach ($List as $r) {
			$no++;
			$row       = array();
			$masreg_id = $r->masreg_id;
			$row[]     = '<a href="#" onclick=pilih_input('.$masreg_id.')><i class="ti-hand-point-up text-primary"></i></a>';
			$row[]     = $no;
			$row[]     = $r->masreg_kode;
			$row[]     = $r->masreg_nama;
			$row[]     = $r->masreg_status;
			
			$data[]    = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->rekening_m->count_all(),
			"recordsFiltered" => $this->rekening_m->count_filtered(),
			"data"            => $data,
		);

		echo json_encode($output);
	}
	function tes_api(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://esign-dev.kuduskab.go.id/api/sign/pdf',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('file'=> new CURLFILE('/D:/Dokuments/file_rekap kehadiran.pdf'),'nik' => '30122019','passphrase' => '#4321qwer*','tampilan' => 'invisible'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Basic ZW9mZmljZTpxd2VydHk=',
				'Cookie: JSESSIONID=179589D225D2691EA2A083320BBC3E02',
				'Content-Type: multipart/form-data',
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		// echo json_decode($response);
		// var_dump($response);
		print_r(json_decode($response));

	}
	function savedata(){
		$post=$this->input->post();
		
		switch ($post['status_input']) {
			case 'insert':
			$this->rekening_m->insert_data($post);
			break;
			case 'update':
			$this->rekening_m->update_data($post);
			break;
			default:
				// code...
			break;
		}
	}
	function deletedata($id){
		$this->rekening_m->deletedata($id);
	}
}

/* End of file Rekening.php */
/* Location: ./application/controllers/master/Rekening.php */