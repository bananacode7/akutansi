<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Unit extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('master/unit_m');
    }

    public function index()
    {
        $this->template->layout('master/unit_v');
    }

    public function data_list()
    {
        $List = $this->unit_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row     = array();
            $unit_id = $r->unit_id;
            $row[]   = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $unit_id . "'" . ')"><i class="ti-pencil text-primary"></i></a>
            <a href  ="javascript:;" onclick="hapusData(' . $unit_id . ')" title="Hapus Data"><i class="ti-trash text-danger"></i></a>';
            $row[]   = $no;
            $row[]   = $r->unit_nama;
            
            $data[]  = $row;

        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->unit_m->count_all(),
            "recordsFiltered" => $this->unit_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function nama_exists($nama)
    {
        $this->db->where('unit_nama', $nama);
        $query = $this->db->get('unit_nama');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_nama_exists()
    {
        if (array_key_exists('nama', $_POST)) {
            if ($this->nama_exists(stripHTMLtags($this->input->post('nama', 'true'))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->unit_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('keuangan_unit', array('unit_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->unit_m->update_data();
    }

    public function deletedata($id)
    {
        $this->unit_m->delete_data($id);
    }
}
/* Location: ./application/controller/master/Jenis_unit.php */
