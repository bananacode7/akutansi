<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Service extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    // Daftar WP
    public function listwajibpajak_post()
    {
        $npwpd = trim($this->post('npwpd'));
        $tahun = trim($this->post('tahun'));

        if ($npwpd == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'No. NPWPD Harap Diisi.',
            ];

            $this->response($response, 403);
        } elseif ($tahun == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Tahun Harap Diisi.',
            ];

            $this->response($response, 403);
        } else {
            // Check ke Data Register
            $dataWP = $this->db->get_where('v_wajibpajak', array('t_npwpd' => $npwpd))->row();
            if (count($dataWP) == 0) {
                $response = [
                    'resp_error' => true,
                    'resp_msg'   => 'Data NPWPD tidak ditemukan',
                ];

                $this->response($response, 404);
            } else {
                $t_idwp = $dataWP->t_idwp;
                $listTagihan = $this->db->order_by('t_tglpendataan', 'asc')->get_where('v_reklame_list', array('t_idwp' => $t_idwp, 't_periodepajak' => $tahun))->result();
                if (count($listTagihan) == 0) {
                    $response = [
                        'resp_error' => true,
                        'resp_msg'   => 'Data Tagihan Reklame Kosong',
                    ];

                    $this->response($response, 404);
                } else {
                    $response = array(
                        'resp_error' => false,
                        'resp_msg'   => 'success',
                        'npwpd'      => trim($dataWP->t_npwpd),
                        'nama_wp'    => trim($dataWP->t_nama),
                        'alamat_wp'  => trim($dataWP->t_alamat),
                    );
                    $response['data'] = array();
                    foreach ($listTagihan as $r) {
                        if ($r->t_tglpembayaran != '') {
                            $status = 'paid';
                        } else {
                            $status = 'notpaid';
                        }

                        $response['data'][] = array(
                            'status'        => $status,
                            'idtransaksi'   => $r->t_idtransaksi,
                            'nop'           => trim($r->t_nop),
                            'nama_op'       => trim($r->t_namaobjekpj),
                            'alamat_op'     => trim($r->t_alamatobjekpj),
                            'jenispajak'    => trim($r->s_namajenis),
                            'tahun'         => $r->t_periodepajak,
                            'tanggal'       => date('d-m-Y', strtotime($r->t_tglpendataan)),
                            'periode'       => date('d-m-Y', strtotime($r->t_masaawal)) . ' - ' . date('d-m-Y', strtotime($r->t_masaakhir)),
                            'jumlahpajak'   => $r->t_jmlhpajak,
                            'tgl_bayar'     => ($r->t_tglpembayaran != '' ? date('d-m-Y', strtotime($r->t_tglpembayaran)) : '-'),
                            'jumlahbayar'   => $r->t_jmlhpembayaran,
                            'jenisreklame'  => strtoupper($r->s_namareklamejenis),
                            'judulreklame'  => trim($r->t_naskah),
                            'lokasireklame' => trim($r->t_lokasi),
                            'panjang'       => $r->t_panjang,
                            'lebar'         => $r->t_lebar,
                            'jangkawaktu'   => $r->t_jangkawaktu,
                            'tipewaktu'     => $r->t_tipewaktu,
                            'kawasan'       => strtoupper($r->s_namareklamekawasan),
                            'konstruksi'    => strtoupper($r->s_namakonstruksi),
                        );
                    }

                    $this->response($response, 200);
                }
            }
        }
    }

    // Detail Transaksi
    public function detailpajakreklame_post()
    {
        $idtransaksi = trim($this->post('idtransaksi'));

        if ($idtransaksi == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'ID Transaksi Kosong.',
            ];

            $this->response($response, 403);
        } else {
            $detail = $this->db->get_where('v_reklame_list', array('t_idtransaksi' => $idtransaksi))->row();
            if (count($detail) == 0) {
                $response = [
                    'resp_error' => true,
                    'resp_msg'   => 'Data Transaksi tidak ditemukan',
                ];

                $this->response($response, 404);
            } else {
                if ($detail->t_tglpembayaran != '') {
                    $status = 'paid';
                } else {
                    $status = 'notpaid';
                }
                $response = [
                    'resp_error'    => false,
                    'resp_msg'      => 'success',
                    'status'        => $status,
                    'nop'           => trim($detail->t_nop),
                    'nama_op'       => trim($detail->t_namaobjekpj),
                    'alamat_op'     => trim($detail->t_alamatobjekpj),
                    'jenispajak'    => trim($detail->s_namajenis),
                    'tahun'         => $detail->t_periodepajak,
                    'tanggal'       => date('d-m-Y', strtotime($detail->t_tglpendataan)),
                    'periode'       => date('d-m-Y', strtotime($detail->t_masaawal)) . ' - ' . date('d-m-Y', strtotime($detail->t_masaakhir)),
                    'jumlahpajak'   => $detail->t_jmlhpajak,
                    'tgl_bayar'     => ($detail->t_tglpembayaran != '' ? date('d-m-Y', strtotime($detail->t_tglpembayaran)) : '-'),
                    'jumlahbayar'   => $detail->t_jmlhpembayaran,
                    'jenisreklame'  => strtoupper($detail->s_namareklamejenis),
                    'judulreklame'  => trim($detail->t_naskah),
                    'lokasireklame' => trim($detail->t_lokasi),
                    'panjang'       => $detail->t_panjang,
                    'lebar'         => $detail->t_lebar,
                    'jangkawaktu'   => $detail->t_jangkawaktu,
                    'tipewaktu'     => $detail->t_tipewaktu,
                    'kawasan'       => strtoupper($detail->s_namareklamekawasan),
                    'konstruksi'    => strtoupper($detail->s_namakonstruksi),
                ];

                $this->response($response, 200);
            }
        }
    }
}
/* Location: ./application/controllers/api/Service.php */
