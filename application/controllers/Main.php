<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	 public function __construct()
    {
        parent::__construct();
        
        $this->load->library('template');
    }

    public function index()
    {
        $this->template->layout('v_main');
  
    }

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */